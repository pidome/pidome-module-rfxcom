/**
 * Presentation classes for the module.
 * <p>
 * The presentations.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom.presentations;
