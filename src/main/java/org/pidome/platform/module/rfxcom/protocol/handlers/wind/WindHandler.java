/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.wind;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.protocol.handlers.DataOnlyHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.STATUS_GROUP_NAME;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.VALUES_GROUP_NAME;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DeviceStringAddress;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.presentation.components.list.DescriptionListItem;

/**
 * Handler for wind data.
 *
 * @author johns
 */
@SuppressWarnings("checkstyle:magicnumber")
public class WindHandler extends DataOnlyHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(WindHandler.class);

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType implements ProtocolDefinition {
        /**
         * WTGR800.
         */
        TYPE1(1, "WTGR800"),
        /**
         * WGR800.
         */
        TYPE2(2, "WGR800"),
        /**
         * STR918, WGR918.
         */
        TYPE3(3, "STR918, WGR918"),
        /**
         * TFA (WIND4).
         */
        TYPE4(4, "TFA (WIND4)"),
        /**
         * UPM WDS500.
         */
        TYPE5(5, "UPM WDS500"),
        /**
         * WS2300.
         */
        TYPE6(6, "WS2300"),
        /**
         * An unknown type.
         */
        UNKNOWN(-1, "Unknown or unhandled type");

        /**
         * Supplying value.
         */
        private final int value;
        /**
         * Description.
         */
        private final String desc;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param bitValue Type value.
         * @param description Type description.
         */
        PacketSubType(final int bitValue, final String description) {
            this.value = bitValue;
            this.desc = description;
        }

        /**
         * @return The enum value
         */
        @Override
        public int getValue() {
            return value;
        }

        /**
         * @return The description.
         */
        @Override
        public String getDescription() {
            return desc;
        }
    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public WindHandler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {

        final PacketSubType subType = getSubPacketTypeByBitId(getParser().getSubType(), PacketSubType.UNKNOWN, PacketSubType.values());
        final byte[] body = this.getParser().getMessageBody();

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            builder.append(BasicPacketParser.decodeSingleByte(body[i]));
        }
        String deviceId = builder.toString();

        int windDir = (Integer.parseInt(BasicPacketParser.decodeSingleByte(body[2]), 16) * 256)
                + Integer.parseInt(BasicPacketParser.decodeSingleByte(body[3]), 16);

        double avgSpeed = 0D;
        double temp = 0D;
        double windTemp = 0D;

        if (!subType.equals(PacketSubType.TYPE5)) {
            avgSpeed = ((Integer.parseInt(BasicPacketParser.decodeSingleByte(body[4]), 16) * 256)
                    + Integer.parseInt(BasicPacketParser.decodeSingleByte(body[5]), 16)) * 0.1;
        }

        double gust = ((Integer.parseInt(BasicPacketParser.decodeSingleByte(body[6]), 16) * 256)
                + Integer.parseInt(BasicPacketParser.decodeSingleByte(body[7]), 16)) * 0.1;

        if (subType.equals(PacketSubType.TYPE4)) {
            temp = OregonBasedGenericImpl.getTemperature(body[8], body[9]);
            windTemp = OregonBasedGenericImpl.getTemperature(body[10], body[11]);
        }

        int batSig = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[12]), 16);

        double bat = OregonBasedGenericImpl.getBatteryStatus(batSig);
        double sig = OregonBasedGenericImpl.getSignalStatus(batSig);

        boolean found = false;

        LOG.debug("Type: {}, Address: {}, Wind speed: {}, Wind direction: {}, Wind gust: {}, Temperature: {}, Wind chill: {}, Battery: {}, Signal: {}", subType.getDescription(), deviceId, avgSpeed, windDir, gust, temp, windTemp, bat, sig);

        for (DeviceAddress deviceAddress : this.getRunningDeviceAddresses(WindDevice.class)) {
            if (deviceAddress.getAddress().equals(deviceId)) {
                found = true;
                DeviceDataNotification notification = new DeviceDataNotification(deviceAddress);
                notification.addData(VALUES_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.TEMPERATURE.name(), temp);

                notification.addData(VALUES_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.WINDSPEED.name(), avgSpeed);
                notification.addData(VALUES_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.WINDGUST.name(), gust);
                notification.addData(VALUES_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.WINDCHILL.name(), windTemp);
                notification.addData(VALUES_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.WINDDIRECTION.name(), windDir);

                notification.addData(STATUS_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.BATTERY.name(), bat);
                notification.addData(STATUS_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.SIGNAL.name(), sig);

                this.sendDataNotification(notification);
            }
        }
        if (!found && this.discoveryEnabled()) {

            DiscoveredDevice<DeviceStringAddress> newDevice = new DiscoveredDevice<>(WindDevice.class, new StringBuilder(subType.getDescription()).append(" or alike").toString());

            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied wind direction", windDir));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied gusts", gust));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied chill", windTemp));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied direction", windDir));
            newDevice.addDescriptionListItem(new DescriptionListItem("Provides", "Wind information"));
            newDevice.addDescriptionListItem(new DescriptionListItem("packettype", ProtocolCollection.PacketType.WIND.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("packetsubtype", subType.toString()));

            newDevice.addFieldParameter("packettype", ProtocolCollection.PacketType.WIND.toString());
            newDevice.addFieldParameter("packetsubtype", subType.toString());

            DeviceStringAddress newAddress = new DeviceStringAddress();
            newAddress.setAddress(deviceId);
            newDevice.setAddress(newAddress);

            this.discovered(newDevice);

        }
    }
}
