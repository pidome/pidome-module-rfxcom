/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.lighting2;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.RFXComSendCommand;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.protocol.handlers.AbstractProtocolHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceCommandRequest;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DeviceStringAddress;
import org.pidome.platform.modules.devices.UnsupportedDeviceCommandException;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.presentation.components.list.DescriptionListItem;

/**
 * Parser for lighting2 devices.
 *
 * @author John
 */
@SuppressWarnings("checkstyle:magicnumber")
public final class Lighting2Handler extends AbstractProtocolHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(Lighting2Handler.class);

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType implements ProtocolDefinition {
        /**
         * AC protocol.
         */
        AC(0, "Learning: KlikAanKlikUit, Anslut, Chacon, CoCo, DI.O, HomeEasy (UK), Intertechno, NEXA"),
        /**
         * HA_EU.
         */
        HE_EU(1, "HomeEasy EU"),
        /**
         * Anslut.
         */
        ANSLUT(2, "Anslut"),
        /**
         * Kambrook.
         */
        KAMBROOK(3, "Kambrook"),
        /**
         * Unknown subtype.
         */
        UNKNOWN(-1, "Unknown type");

        /**
         * subtype value.
         */
        private final int value;
        /**
         * subtype description.
         */
        private final String desc;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param bitValue Type value.
         * @param description Type description.
         */
        PacketSubType(final int bitValue, final String description) {
            this.value = bitValue;
            this.desc = description;
        }

        /**
         * @return Teh subtype value
         */
        @Override
        public int getValue() {
            return value;
        }

        /**
         * @return The subtype description.
         */
        @Override
        public String getDescription() {
            return desc;
        }

    }

    /**
     * The default group name used.
     */
    public static final String DEFAULT_GROUP_NAME = "deviceactions";

    /**
     * Names of the controls used.
     */
    private enum ControlIdName {
        /**
         * One device switch.
         */
        SWITCH,
        /**
         * Grouped switch.
         */
        GROUPSWITCH,
        /**
         * One device dim level.
         */
        DIMLEVEL,
        /**
         * Grouped dim level.
         */
        GROUPDIMLEVEL
    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public Lighting2Handler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * Handles the received data.
     *
     * @throws PacketProtocolException when the protocol handling fails.
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {
        final PacketSubType subType = getSubPacketTypeByBitId(getParser().getSubType(), PacketSubType.UNKNOWN, PacketSubType.values());
        final byte[] data = this.getParser().getMessageBody();
        String[] deviceId = new String[4];
        for (int i = 0; i < 4; i++) {
            deviceId[i] = BasicPacketParser.decodeSingleByte(data[i]);
        }
        String baseAddress = BasicPacketParser.decodeSingleByte(data[4]);
        String address = new StringBuilder(String.join(",", deviceId)).append(":").append(baseAddress).toString();
        boolean found = false;
        for (DeviceAddress deviceAddress : this.getRunningDeviceAddresses(Lighting2Device.class)) {
            if (deviceAddress.getAddress().equals(address)) {
                found = true;
                DeviceDataNotification notification = new DeviceDataNotification(deviceAddress);
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Setting data for command type: {}", BasicPacketParser.decodeSingleByte(data[5]));
                }
                try {
                    switch (data[5]) {
                        case 0x00:
                            notification.addData(DEFAULT_GROUP_NAME, ControlIdName.SWITCH.name(), false);
                            break;
                        case 0x01:
                            notification.addData(DEFAULT_GROUP_NAME, ControlIdName.SWITCH.name(), true);
                            break;
                        case 0x02:
                            notification.addData(DEFAULT_GROUP_NAME, ControlIdName.DIMLEVEL.name(),
                                    Integer.parseInt(BasicPacketParser.decodeSingleByte(data[6]))
                            );
                            break;
                        case 0x03:
                            notification.addData(DEFAULT_GROUP_NAME, ControlIdName.GROUPSWITCH.name(), false);
                            break;
                        case 0x04:
                            notification.addData(DEFAULT_GROUP_NAME, ControlIdName.GROUPSWITCH.name(), true);
                            break;
                        case 0x05:
                            notification.addData(DEFAULT_GROUP_NAME, ControlIdName.GROUPDIMLEVEL.name(),
                                    Integer.parseInt(BasicPacketParser.decodeSingleByte(data[6]))
                            );
                            break;
                        default:
                            throw new UnsupportedDeviceCommandException("Unsupported command: " + BasicPacketParser.decodeSingleByte(data[5]));
                    }
                    this.sendDataNotification(notification);
                } catch (UnsupportedDeviceCommandException ex) {
                    LOG.error("Unknown device command [{}]", ex.getMessage());
                }
                break;
            }
        }
        if (!found && this.discoveryEnabled()) {

            DiscoveredDevice<DeviceStringAddress> newDevice = new DiscoveredDevice<>(Lighting2Device.class, new StringBuilder(subType.getDescription()).append(" or alike").toString());

            newDevice.addDescriptionListItem(new DescriptionListItem("Remote address", baseAddress));
            newDevice.addDescriptionListItem(new DescriptionListItem("Provides", "On/Off,Dimming,Group etc.. depending on the device discovered"));
            newDevice.addDescriptionListItem(new DescriptionListItem("Packet type", ProtocolCollection.PacketType.LIGHTING_2.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("Packet subtype", subType.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("Level (id supported)", data[8]));
            newDevice.addDescriptionListItem(new DescriptionListItem("Signal strength", (data[9] >> 4) & 0xf));

            newDevice.addFieldParameter("packettype", ProtocolCollection.PacketType.LIGHTING_2.toString());
            newDevice.addFieldParameter("packetsubtype", subType.toString());

            DeviceStringAddress newAddress = new DeviceStringAddress();
            newAddress.setAddress(baseAddress);
            newDevice.setAddress(newAddress);

            LOG.info("Pushing to discovery: [{}]", newDevice);

            this.discovered(newDevice);

            LOG.info(newDevice);

        }
    }

    /**
     * Handles data from a device request.
     *
     * @param request The request to be done.
     * @param promise The promise to succeed or fail with an exception.
     * @return Future for the success or failure indication.
     */
    @Override
    public Future<Void> handleReceivedFromDevice(final DeviceCommandRequest request, final Promise<Void> promise) {
        LOG.info("Request done: [{}]", request);
        request.getDevice().getParameters().get("packetsubtype").ifPresentOrElse(parameter -> {
            try {
                byte level = (byte) 0x0F;
                byte command = (byte) 0x00;
                switch (ControlIdName.valueOf(request.getControlId())) {
                    case SWITCH:
                        if ((boolean) request.getCommandValue()) {
                            command = (byte) 0x01;
                        } else {
                            command = (byte) 0x00;
                        }
                        break;
                    case GROUPSWITCH:
                        if ((boolean) request.getCommandValue()) {
                            command = (byte) 0x04;
                        } else {
                            command = (byte) 0x03;
                        }
                        break;
                    case DIMLEVEL:
                        command = (byte) 0x02;
                        level = createDimLevel(Integer.parseInt(request.getCommandValue().toString()));
                        break;
                    case GROUPDIMLEVEL:
                        command = (byte) 0x05;
                        level = createDimLevel(Integer.parseInt(request.getCommandValue().toString()));
                        break;
                    default:
                        throw new UnsupportedDeviceCommandException("Unknown control id " + request.getControlId());
                }
                String[] idLocation = ((String) request.getDevice().getAddress().getAddress()).split(":");
                byte unitCode = (byte) Integer.parseInt(idLocation[1], 16);
                String[] id = idLocation[0].split(",");
                byte[] idSet = new byte[4];
                for (int i = 0; i < id.length; i++) {
                    idSet[i] = (byte) Integer.parseInt(id[i], 16);
                }
                byte[] toSend = new byte[]{
                    (byte) 0x0B,
                    (byte) 0x11,
                    (byte) getSubPacketTypeIntByStringId(parameter.getValue(), PacketSubType.UNKNOWN, PacketSubType.values()),
                    (byte) 0x01,
                    idSet[0],
                    idSet[1],
                    idSet[2],
                    idSet[3],
                    unitCode,
                    command,
                    level,
                    (byte) 0x00};
                RFXComSendCommand rfxSend = new RFXComSendCommand(toSend, 0);
                this.sendToRFXCom(rfxSend);
                promise.complete();
            } catch (PacketProtocolException ex) {
                LOG.error("Error in protocol handling", ex);
                promise.fail(ex);
            } catch (UnsupportedDeviceCommandException ex) {
                LOG.error("Error in device command handling handling", ex);
                promise.fail(ex);
            }
        }, () -> {
            promise.fail(new Exception("No subtype present to identify type with: " + request.getDevice().getParameters().get("packetsubtype")));
        });
        return promise.future();
    }

    /**
     * Creates the dim level (0-100 -> 0 - 15).
     *
     * @param value percentage 0 - 100.
     * @return The dime level.
     * @throws PacketProtocolException When dim level is out of bounds.
     */
    private byte createDimLevel(final int value) throws PacketProtocolException {
        if (value > 100 || value < 0) {
            throw new PacketProtocolException("Impossible dim value: " + value);
        } else {
            int level = value / 6;
            switch (level) {
                case 0:
                    return (byte) 0x00;
                case 1:
                    return (byte) 0x01;
                case 2:
                    return (byte) 0x02;
                case 3:
                    return (byte) 0x03;
                case 4:
                    return (byte) 0x04;
                case 5:
                    return (byte) 0x05;
                case 6:
                    return (byte) 0x06;
                case 7:
                    return (byte) 0x07;
                case 8:
                    return (byte) 0x08;
                case 9:
                    return (byte) 0x09;
                case 10:
                    return (byte) 0x0A;
                case 11:
                    return (byte) 0x0B;
                case 12:
                    return (byte) 0x0C;
                case 13:
                    return (byte) 0x0D;
                case 14:
                    return (byte) 0x0E;
                default:
                    return (byte) 0x0F;
            }
        }
    }

}
