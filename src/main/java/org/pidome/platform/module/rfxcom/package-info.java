/**
 * Generic classes for the RFXCom module.
 * <p>
 * The module.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom;
