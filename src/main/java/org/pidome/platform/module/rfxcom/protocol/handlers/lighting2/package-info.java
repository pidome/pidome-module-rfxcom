/**
 * Handlers and devices for Lighting2 based devices.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.lighting2;
