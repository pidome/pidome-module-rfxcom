/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.humidity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.protocol.handlers.DataOnlyHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.OregonDefaultControlNames;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.STATUS_GROUP_NAME;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.VALUES_GROUP_NAME;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DeviceStringAddress;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.presentation.components.list.DescriptionListItem;

/**
 * The humidity handler.
 *
 * @author johns
 */
@SuppressWarnings("checkstyle:magicnumber")
public class HumidityHandler extends DataOnlyHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(HumidityHandler.class);

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType implements ProtocolDefinition {
        /**
         * Unknown type.
         */
        UNKNOWN(-1, "Unknown type"),
        /**
         * LaCrosse TX3.
         */
        TYPE1(1, "LaCrosse TX3");

        /**
         * Supplying value.
         */
        private final int value;
        /**
         * Description.
         */
        private final String desc;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param subType The subtype.
         * @param description The description.
         */
        PacketSubType(final int subType, final String description) {
            this.value = subType;
            this.desc = description;
        }

        /**
         * @return The enum value.
         */
        @Override
        public int getValue() {
            return value;
        }

        /**
         * @return The description.
         */
        @Override
        public String getDescription() {
            return desc;
        }

    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public HumidityHandler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {

        final PacketSubType subType = getSubPacketTypeByBitId(getParser().getSubType(), PacketSubType.UNKNOWN, PacketSubType.values());
        final byte[] body = this.getParser().getMessageBody();

        int hum;
        int humstat;

        int batSig;

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            builder.append(BasicPacketParser.decodeSingleByte(body[i]));
        }
        String deviceId = builder.toString();

        int remoteChannel = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[1]), 16);

        hum = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[2]), 16);
        humstat = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[3]), 16);
        batSig = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[4]), 16);

        double bat = OregonBasedGenericImpl.getBatteryStatus(batSig);
        double sig = OregonBasedGenericImpl.getSignalStatus(batSig);

        LOG.debug("Address: {}, Channel: {}, Humidity: {}, hum stat: {}, Battery: {}, Signal: {}", deviceId, remoteChannel, hum, humstat, bat, sig);

        boolean found = false;
        for (DeviceAddress deviceAddress : this.getRunningDeviceAddresses(HumidityDevice.class)) {
            if (deviceAddress.getAddress().equals(deviceId)) {
                found = true;
                DeviceDataNotification notification = new DeviceDataNotification(deviceAddress);
                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.HUMIDITY.name(), hum);
                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.HUMIDITYTEXT.name(), OregonBasedGenericImpl.getHumidityText(humstat));

                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.BATTERY.name(), bat);
                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.SIGNAL.name(), sig);
                this.sendDataNotification(notification);
            }
        }
        if (!found && this.discoveryEnabled()) {

            DiscoveredDevice<DeviceStringAddress> newDevice = new DiscoveredDevice<>(HumidityDevice.class, new StringBuilder(subType.getDescription()).append(" or alike").toString());

            newDevice.addDescriptionListItem(new DescriptionListItem("Channel", remoteChannel));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied humudity", hum));
            newDevice.addDescriptionListItem(new DescriptionListItem("Provides", "Humidity"));
            newDevice.addDescriptionListItem(new DescriptionListItem("packettype", ProtocolCollection.PacketType.HUMIDITY.name()));
            newDevice.addDescriptionListItem(new DescriptionListItem("packetsubtype", subType.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("Channel", remoteChannel));
            newDevice.addDescriptionListItem(new DescriptionListItem("Channel", remoteChannel));

            DeviceStringAddress address = new DeviceStringAddress();
            address.setAddress(deviceId);
            newDevice.setAddress(address);

            newDevice.addFieldParameter("packettype", ProtocolCollection.PacketType.HUMIDITY.name());
            newDevice.addFieldParameter("packetsubtype", subType.toString());

            this.discovered(newDevice);
        }
    }

}
