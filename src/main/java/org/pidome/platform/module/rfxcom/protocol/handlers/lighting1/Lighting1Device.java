/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.lighting1;

import org.pidome.platform.module.rfxcom.protocol.AbstractRFXComDevice;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.modules.devices.DeviceStringAddress;

/**
 * Device for handling lighting1.
 *
 * @author John
 */
public class Lighting1Device extends AbstractRFXComDevice<DeviceStringAddress> {

    /**
     * The lighting 1 subtype.
     */
    private Lighting1Handler.PacketSubType subType;

    /**
     * {@inheritDoc}
     */
    @Override
    public ProtocolCollection.PacketType getPacketType() {
        return ProtocolCollection.PacketType.LIGHTING_1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPacketSubType() throws PacketProtocolException {
        return this.subType.getValue();
    }

    /**
     * If there are any tasks or what so ever used, use this to stop them.
     */
    @Override
    public void shutdownDevice() {
        /// not used.
    }

    /**
     * If there are any automated tasks or what so ever use this to start them.
     */
    @Override
    public void startupDevice() {
        this.getParameters().get("subtype").ifPresentOrElse(parameter -> {
            this.subType = Lighting1Handler.PacketSubType.valueOf(parameter.getValue());
        }, () -> {
            this.subType = Lighting1Handler.PacketSubType.UNKNOWN;
        });
    }

}
