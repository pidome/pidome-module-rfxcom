/**
 * Base handlers.
 *
 * <p>
 * Base handlers for both protocol and handlers implementations.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom.protocol.handlers;
