/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.rfxcom;

/**
 * Protocol status mapping.
 *
 * This is only the supported protocols. It is possible protocols are used in
 * multiple handlers, it depends on data structure.
 *
 * @author johns
 */
public enum RFXComProtocol {

    /**
     * Blyss.
     */
    BLYSS("AE Blyss, Cuveo", 0x01, 3),
    /**
     * Rubicson.
     */
    RUBICSON("Rubicson, Alecto, Banggood", 0x02, 3),
    /**
     * FineOffset.
     */
    FINEOFFSET("FineOffset, Viking, XT300", 0x04, 3),
    /**
     * Lighting4.
     */
    LIGHTING4("Lighting4", 0x08, 3),
    /**
     * RSL.
     */
    RSL("RSL, Revolt", 0x10, 3),
    /**
     * Byron SX.
     */
    BYRONSX("Byron SX/BY, SelectPlus", 0x20, 3),
    /**
     * Imagintronix.
     */
    IMAGINTRONIX("Imagintronix, Opus", 0x40, 3),
    /**
     * Undecoded.
     */
    UNDECODED("Undec", 0x80, 3),
    /**
     * Mertik.
     */
    MERTIK("Mertik", 0x01, 4),
    /**
     * LightwaveRF.
     */
    LIGHTWAVERF("AD LightwaveRF, Livolo", 0x02, 4),
    /**
     * Hideki.
     */
    HIDEKI("Hideki, TFA, Cresta, UPM", 0x04, 4),
    /**
     * La Crosse.
     */
    LACROSSE("La Crosse", 0x08, 4),
    /**
     * FS20.
     */
    FS20("FS20", 0x10, 4),
    /**
     * Proguard.
     */
    PROGUARD("Proguard", 0x20, 4),
    /**
     * BlindsT0.
     */
    BLINDST0("BlindsT0", 0x40, 4),
    /**
     * BlindsT1.
     */
    BLINDST1("BlindsT1", 0x80, 4),
    /**
     * X10.
     */
    X10("X10", 0x01, 5),
    /**
     * ARC.
     */
    ARC("ARC", 0x02, 5),
    /**
     * AC.
     */
    AC("AC", 0x04, 5),
    /**
     * HomeEasy EU.
     */
    HOMEEASY("HomeEasy EU", 0x08, 5),
    /**
     * Meiantech.
     */
    MEIANTECH("Meiantech", 0x10, 5),
    /**
     * Oregon Scientific.
     */
    OREGON("Oregon Scientific", 0x20, 5),
    /**
     * ATI.
     */
    ATI("ATI, Cartelectronic", 0x40, 5),
    /**
     * Visonic.
     */
    VISONIC("Visonic", 0x80, 5),
    /**
     * KeeLoq.
     */
    KEELOQ("KeeLoq", 0x01, 6),
    /**
     * HomeConfort.
     */
    HOMECONFORT("HomeConfort,Fan", 0x02, 6),
    /**
     * MCZ.
     */
    MCZ("MCZ", 0x40, 6),
    /**
     * Funkbus.
     */
    FUNKBUS("Funkbus", 0x80, 6);

    /**
     * Descriptive name.
     */
    private final String name;

    /**
     * Bit.
     */
    private final int bit;

    /**
     * Bit position.
     */
    private final int bitPos;

    /**
     * Enum constructor.
     *
     * @param descName The descriptive name.
     * @param bitVal The bit.
     * @param bitValPos The position in the status message.
     */
    RFXComProtocol(final String descName, final int bitVal, final int bitValPos) {
        this.name = descName;
        this.bit = bitVal;
        this.bitPos = bitValPos;
    }

    /**
     * Descriptive name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Bit.
     *
     * @return the bit
     */
    public int getBit() {
        return bit;
    }

    /**
     * Bit position.
     *
     * @return the bitPos
     */
    public int getBitPos() {
        return bitPos;
    }

}
