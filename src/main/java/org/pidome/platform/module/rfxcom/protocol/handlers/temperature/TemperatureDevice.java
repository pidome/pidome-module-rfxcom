/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.temperature;

import org.pidome.platform.module.rfxcom.protocol.AbstractRFXComDevice;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.modules.devices.DeviceStringAddress;

/**
 * Device for only temperature.
 *
 * @author John
 */
public class TemperatureDevice extends AbstractRFXComDevice<DeviceStringAddress> {

    /**
     * Class logger.
     */
    private TemperatureHandler.PacketSubType subType = TemperatureHandler.PacketSubType.TYPE1;

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPacketSubType() {
        return subType.getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProtocolCollection.PacketType getPacketType() {
        return ProtocolCollection.PacketType.TEMPERATURE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void shutdownDevice() {
        /// not used.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startupDevice() {
        this.getParameters().get("subtype").ifPresentOrElse(parameter -> {
            this.subType = TemperatureHandler.PacketSubType.valueOf(parameter.getValue());
        }, () -> {
            this.subType = TemperatureHandler.PacketSubType.UNKNOWN;
        });
    }

}
