/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.utils;

/**
 * Exception thrown from the packet parser.
 *
 * @author John
 */
public class BasicPacketParserException extends Exception {

    /**
     * Creates a new instance of <code>RFXComBasicPacketParserException</code>
     * without detail message.
     */
    public BasicPacketParserException() {
    }

    /**
     * Constructs an instance of <code>RFXComBasicPacketParserException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public BasicPacketParserException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>RFXComBasicPacketParserException</code>
     * with the specified detail message and original exception.
     *
     * @param msg the detail message.
     * @param thrown The original exception.
     */
    public BasicPacketParserException(final String msg, final Throwable thrown) {
        super(msg, thrown);
    }

}
