/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol;

/**
 * Protocol definition to be implemented by protocol subtype definitions.
 *
 * @author johns
 */
public interface ProtocolDefinition {

    /**
     * Returns the enum bit value.
     *
     * @return The bit value.
     */
    int getValue();

    /**
     * Returns the exact name of the definition.
     *
     * @return The exact name.
     */
    String name();

    /**
     * Returns the description.
     *
     * @return The description.
     */
    String getDescription();

}
