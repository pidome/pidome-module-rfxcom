/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.rfxcom;

/**
 * subtypes for the interface protocol.
 *
 * @author johns
 */
public enum InterfaceProtocol {

    /**
     * RFXCom mode response.
     */
    RFXCOM_MODE(0x00),
    /**
     * Receiver start.
     */
    RFXCOM_START(0x07),
    /**
     * RFXCom unknown command response.
     */
    RFXCOM_UNKNOWN(0xff);

    /**
     * Supplying value.
     */
    private final int value;

    /**
     * Enum constructor for supplying a value to the enum.
     *
     * @param protoValue The byte value.
     */
    InterfaceProtocol(final int protoValue) {
        this.value = protoValue;
    }

    /**
     * Returns the enum value.
     *
     * @return The byte value.
     */
    public int getValue() {
        return value;
    }

    /**
     * Returns an enum by the byte value.
     *
     * @param byteValue The value
     * @return The enum.
     */
    public static InterfaceProtocol byByte(final int byteValue) {
        for (InterfaceProtocol proto : InterfaceProtocol.values()) {
            if (proto.getValue() == byteValue) {
                return proto;
            }
        }
        return RFXCOM_UNKNOWN;
    }

}
