/**
 * Handlers and devices for temperature based devices.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.temperature;
