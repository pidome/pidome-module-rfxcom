/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.rain;

import org.pidome.platform.module.rfxcom.protocol.AbstractRFXComDevice;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.modules.devices.DeviceStringAddress;

/**
 * Device for rain.
 *
 * @author John
 */
public class RainDevice extends AbstractRFXComDevice<DeviceStringAddress> {

    /**
     * The subtype.
     */
    private RainHandler.PacketSubType subType = RainHandler.PacketSubType.TYPE1;

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPacketSubType() throws PacketProtocolException {
        return subType.getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProtocolCollection.PacketType getPacketType() {
        return ProtocolCollection.PacketType.RAIN;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void shutdownDevice() {
        /// not used.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startupDevice() {
        this.getParameters().get("subtype").ifPresentOrElse(parameter -> {
            this.subType = RainHandler.PacketSubType.valueOf(parameter.getValue());
        }, () -> {
            this.subType = RainHandler.PacketSubType.UNKNOWN;
        });
    }

}
