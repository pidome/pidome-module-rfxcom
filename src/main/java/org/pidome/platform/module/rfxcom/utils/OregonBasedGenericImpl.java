/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Some tool methods based on the OREGON protocol.
 *
 * @author johns
 */
@SuppressWarnings("checkstyle:magicnumber")
public final class OregonBasedGenericImpl {

    /**
     * Utility class, no public constructor.
     */
    private OregonBasedGenericImpl() {
        /// private default constructor.
    }

    /**
     * The default values name.
     */
    public static final String VALUES_GROUP_NAME = "VALUES";

    /**
     * The default status name.
     */
    public static final String STATUS_GROUP_NAME = "VALUES";

    /**
     * Generic control names used.
     */
    public enum OregonDefaultControlNames {
        /**
         * The pressure measured.
         */
        PRESSURE,
        /**
         * Forecast text.
         */
        FORECASTTEXT,
        /**
         * The wind speed.
         */
        WINDSPEED,
        /**
         * The wind gusts.
         */
        WINDGUST,
        /**
         * The wind chill.
         */
        WINDCHILL,
        /**
         * The wind direction.
         */
        WINDDIRECTION,
        /**
         * Total rain.
         */
        RAIN_TOTAL,
        /**
         * Current rain.
         */
        RAIN_CURRENT,
        /**
         * Humidity.
         */
        HUMIDITY,
        /**
         * Humidity as text.
         */
        HUMIDITYTEXT,
        /**
         * The temperature.
         */
        TEMPERATURE,
        /**
         * Battery strength.
         */
        BATTERY,
        /**
         * Signal strength.
         */
        SIGNAL;
    }

    /**
     * Calculate temperature.
     *
     * @param high The high byte.
     * @param low The low byte.
     * @return The temperature.
     */
    public static double getTemperature(final byte high, final byte low) {
        double temp = ((high & 0x7F) << 8 | (low & 0xFF)) * 0.1;
        if ((high & 0x80) != 0) {
            return -new BigDecimal(temp).setScale(2, RoundingMode.HALF_UP).doubleValue();
        } else {
            return new BigDecimal(temp).setScale(2, RoundingMode.HALF_UP).doubleValue();
        }
    }

    /**
     * Return pressure.
     *
     * @param high The high byte.
     * @param low The low byte.
     * @return The pressure.
     */
    public static double getPressure(final byte high, final byte low) {
        int barHigh = Integer.parseInt(BasicPacketParser.decodeSingleByte(high), 16);
        int barLow = Integer.parseInt(BasicPacketParser.decodeSingleByte(low), 16);
        int finalBarHigh = barHigh & ~(1 << 7) << 8;
        return new BigDecimal((finalBarHigh + barLow)).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Returns battery status in percentages.
     *
     * @param battCombiInt Integer with battery value.
     * @return The battery status in percentage.
     */
    public static double getBatteryStatus(final int battCombiInt) {
        return (battCombiInt & 0x0F) * 10.0;
    }

    /**
     * Get signal strength/status.
     *
     * @param strengthCombiInt Integer for signal strength
     * @return The signal strength.
     */
    public static int getSignalStatus(final int strengthCombiInt) {
        return (strengthCombiInt & 0xF0) >> 4;
    }

    /**
     * Returns a forecast text based on int value.
     *
     * @param forecast The forecast int value.
     * @return The forecast text.
     */
    public static String getForecastText(final int forecast) {
        switch (forecast) {
            case 0:
                return "Sunny";
            case 1:
                return "Partly cloudy";
            case 2:
                return "Cloudy";
            case 3:
                return "Rainy";
            default:
                return "Unknown";
        }
    }

    /**
     * Returns humidy text based in given int index.
     *
     * @param humInt The index to give.
     * @return The text.
     */
    public static String getHumidityText(final int humInt) {
        switch (humInt) {
            case 0:
                return "Dry";
            case 1:
                return "Comfort";
            case 2:
                return "Normal";
            case 3:
                return "Wet";
            default:
                return "Unknown";
        }
    }

}
