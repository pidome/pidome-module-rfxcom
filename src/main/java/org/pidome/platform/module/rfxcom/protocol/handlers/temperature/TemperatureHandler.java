/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.temperature;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.protocol.handlers.DataOnlyHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.OregonDefaultControlNames;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.STATUS_GROUP_NAME;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.VALUES_GROUP_NAME;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DeviceStringAddress;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.presentation.components.list.DescriptionListItem;

/**
 * Temperature devices handler.
 *
 * @author johns
 */
@SuppressWarnings("checkstyle:magicnumber")
public class TemperatureHandler extends DataOnlyHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(TemperatureHandler.class);

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType implements ProtocolDefinition {
        /**
         * THR128/138, THC138.
         */
        TYPE1(1, "THR128/138, THC138"),
        /**
         * THC238/268,THN132,THWR288,THRN122,THN122,AW129/131.
         */
        TYPE2(2, "THC238/268,THN132,THWR288,THRN122,THN122,AW129/131"),
        /**
         * THWR800.
         */
        TYPE3(3, "THWR800"),
        /**
         * RTHN318.
         */
        TYPE4(4, "RTHN318"),
        /**
         * La Crosse TX3, TX4, TX17.
         */
        TYPE5(5, "La Crosse TX3, TX4, TX17"),
        /**
         * TS15C.
         */
        TYPE6(6, "TS15C"),
        /**
         * Viking 02811.
         */
        TYPE7(7, "Viking 02811"),
        /**
         * La Crosse WS2300La.
         */
        TYPE8(8, "La Crosse WS2300La"),
        /**
         * RUBiCSON.
         */
        TYPE9(9, "RUBiCSON"),
        /**
         * TFA 30.3133.
         */
        TYPE10(10, "TFA 30.3133"),
        /**
         * An unknown type.
         */
        UNKNOWN(-1, "Unknown or unhandled type");

        /**
         * Supplying value.
         */
        private final int value;
        /**
         * Description.
         */
        private final String desc;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param bitValue Type value.
         * @param description Type description.
         */
        PacketSubType(final int bitValue, final String description) {
            this.value = bitValue;
            this.desc = description;
        }

        /**
         * @return The enum value
         */
        @Override
        public int getValue() {
            return value;
        }

        /**
         * @return The description.
         */
        @Override
        public String getDescription() {
            return desc;
        }
    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public TemperatureHandler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {

        final PacketSubType subType = getSubPacketTypeByBitId(getParser().getSubType(), PacketSubType.UNKNOWN, PacketSubType.values());
        final byte[] body = this.getParser().getMessageBody();

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            builder.append(BasicPacketParser.decodeSingleByte(body[i]));
        }

        String deviceId = builder.toString();
        int remoteChannel = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[1]), 16);
        double temp = OregonBasedGenericImpl.getTemperature(body[2], body[3]);
        int batSig = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[4]), 16);
        double bat = OregonBasedGenericImpl.getBatteryStatus(batSig);
        double sig = OregonBasedGenericImpl.getSignalStatus(batSig);

        boolean found = false;

        LOG.debug("Address: {}, Channel: {}, Temp: {}, Battery: {}, Signal: {}", deviceId, remoteChannel, temp, bat, sig);

        for (DeviceAddress deviceAddress : this.getRunningDeviceAddresses(TemperatureDevice.class)) {
            if (deviceAddress.getAddress().equals(deviceId)) {
                found = true;
                DeviceDataNotification notification = new DeviceDataNotification(deviceAddress);
                notification.addData(VALUES_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.TEMPERATURE.name(), temp);

                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.BATTERY.name(), bat);
                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.SIGNAL.name(), sig);
                this.sendDataNotification(notification);
            }
        }
        if (!found && this.discoveryEnabled()) {

            DiscoveredDevice<DeviceStringAddress> newDevice = new DiscoveredDevice<>(TemperatureDevice.class, new StringBuilder(subType.getDescription()).append(" or alike").toString());

            newDevice.addDescriptionListItem(new DescriptionListItem("Channel", remoteChannel));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied temp", temp));
            newDevice.addDescriptionListItem(new DescriptionListItem("Provides", "Temperature"));
            newDevice.addDescriptionListItem(new DescriptionListItem("packettype", ProtocolCollection.PacketType.TEMPERATURE.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("packetsubtype", subType.toString()));

            newDevice.addFieldParameter("packettype", ProtocolCollection.PacketType.TEMPERATURE.toString());
            newDevice.addFieldParameter("packetsubtype", subType.toString());

            DeviceStringAddress newAddress = new DeviceStringAddress();
            newAddress.setAddress(deviceId);
            newDevice.setAddress(newAddress);

            this.discovered(newDevice);

        }
    }
}
