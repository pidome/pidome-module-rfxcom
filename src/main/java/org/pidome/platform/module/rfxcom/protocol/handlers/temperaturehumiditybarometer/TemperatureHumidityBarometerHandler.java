/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.temperaturehumiditybarometer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.protocol.handlers.DataOnlyHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.OregonDefaultControlNames;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.STATUS_GROUP_NAME;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.VALUES_GROUP_NAME;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DeviceStringAddress;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.presentation.components.list.DescriptionListItem;

/**
 * Handler for temperature, humidity and pressure.
 *
 * @author johns
 */
@SuppressWarnings("checkstyle:magicnumber")
public class TemperatureHumidityBarometerHandler extends DataOnlyHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(TemperatureHumidityBarometerHandler.class);

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType implements ProtocolDefinition {
        /**
         * BTHR918.
         */
        TYPE1(1, "BTHR918"),
        /**
         * BTHR918N, BTHR968.
         */
        TYPE2(2, "BTHR918N, BTHR968"),
        /**
         * An unknown type.
         */
        UNKNOWN(-1, "Unknown or unhandled type");

        /**
         * Supplying value.
         */
        private final int value;
        /**
         * Description.
         */
        private final String desc;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param bitValue Type value.
         * @param description Type description.
         */
        PacketSubType(final int bitValue, final String description) {
            this.value = bitValue;
            this.desc = description;
        }

        /**
         * @return The enum value
         */
        @Override
        public int getValue() {
            return value;
        }

        /**
         * @return The description.
         */
        @Override
        public String getDescription() {
            return desc;
        }
    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public TemperatureHumidityBarometerHandler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {

        final PacketSubType subType = getSubPacketTypeByBitId(getParser().getSubType(), PacketSubType.UNKNOWN, PacketSubType.values());
        final byte[] body = this.getParser().getMessageBody();

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            builder.append(BasicPacketParser.decodeSingleByte(body[i]));
        }

        String deviceId = builder.toString();
        int remoteChannel = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[1]), 16);
        double temp = OregonBasedGenericImpl.getTemperature(body[2], body[3]);

        int hum = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[4]), 16);
        int humstat = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[5]), 16);

        double barLevel = OregonBasedGenericImpl.getPressure(body[6], body[7]);
        int forecast = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[8]), 16);

        int batSig = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[9]), 16);

        double bat = OregonBasedGenericImpl.getBatteryStatus(batSig);
        double sig = OregonBasedGenericImpl.getSignalStatus(batSig);

        boolean found = false;

        LOG.debug("Address: {}, Channel: {}, Temp: {}, Humidity: {}, Humidity txt const: {}, Pressure: {}, forecast: {}, Battery: {}, Signal: {}", deviceId, remoteChannel, temp, hum, humstat, barLevel, forecast, bat, sig);

        for (DeviceAddress deviceAddress : this.getRunningDeviceAddresses(TemperatureHumidityBarometerDevice.class)) {
            if (deviceAddress.getAddress().equals(deviceId)) {
                found = true;
                DeviceDataNotification notification = new DeviceDataNotification(deviceAddress);

                notification.addData(VALUES_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.TEMPERATURE.name(), temp);

                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.HUMIDITY.name(), hum);
                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.HUMIDITYTEXT.name(), OregonBasedGenericImpl.getHumidityText(humstat));

                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.PRESSURE.name(), barLevel);
                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.FORECASTTEXT.name(), OregonBasedGenericImpl.getForecastText(forecast));

                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.BATTERY.name(), bat);
                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.SIGNAL.name(), sig);

                this.sendDataNotification(notification);
            }
        }
        if (!found && this.discoveryEnabled()) {

            DiscoveredDevice<DeviceStringAddress> newDevice = new DiscoveredDevice<>(TemperatureHumidityBarometerDevice.class, new StringBuilder(subType.getDescription()).append(" or alike").toString());

            newDevice.addDescriptionListItem(new DescriptionListItem("Channel", remoteChannel));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied temp", temp));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied humudity", hum));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied pressure", barLevel));
            newDevice.addDescriptionListItem(new DescriptionListItem("Provides", "Temperature, humidity and pressure"));
            newDevice.addDescriptionListItem(new DescriptionListItem("packettype", ProtocolCollection.PacketType.TEMPERATURE_HUMIDITY_BAROMETER.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("packetsubtype", subType.toString()));

            newDevice.addFieldParameter("packettype", ProtocolCollection.PacketType.TEMPERATURE_HUMIDITY_BAROMETER.toString());
            newDevice.addFieldParameter("packetsubtype", subType.toString());

            DeviceStringAddress newAddress = new DeviceStringAddress();
            newAddress.setAddress(deviceId);
            newDevice.setAddress(newAddress);

            this.discovered(newDevice);

        }
    }

}
