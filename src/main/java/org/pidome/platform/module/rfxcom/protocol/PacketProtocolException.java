/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol;

/**
 * Exception thrown from handlers when handling of the protocol fails.
 *
 * @author John
 */
public class PacketProtocolException extends Exception {

    /**
     * Creates a new instance of <code>PacketProtocolException</code> without
     * detail message.
     */
    public PacketProtocolException() {
    }

    /**
     * Constructs an instance of <code>PacketProtocolException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public PacketProtocolException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>PacketProtocolException</code> with the
     * specified detail message and cause.
     *
     * @param msg The detail message.
     * @param cause The cause of the exception.
     */
    public PacketProtocolException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

}
