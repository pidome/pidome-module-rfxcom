/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom;

/**
 * Contains the data to be send to the hardware.
 *
 * @author johns
 */
public class RFXComSendCommand {

    /**
     * The data to send.
     */
    private final byte[] data;

    /**
     * The delay time in milliseconds.
     */
    private final int delayed;

    /**
     * Constructor for sending data.
     *
     * @param sendData The data to send.
     * @param sendDelay The delay time to send in milliseconds.
     */
    public RFXComSendCommand(final byte[] sendData, final int sendDelay) {
        this.data = sendData;
        this.delayed = sendDelay;
    }

    /**
     * The data to send.
     *
     * @return the data
     */
    public byte[] getData() {
        return data;
    }

    /**
     * The delay time in milliseconds.
     *
     * @return the delayed
     */
    public int getDelayed() {
        return delayed;
    }

}
