/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.rfxcom;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.RFXComSendCommand;
import org.pidome.platform.module.rfxcom.presentations.GenericInfoPresentation;
import static org.pidome.platform.module.rfxcom.presentations.GenericInfoPresentation.FIRMWARE_TYPES;
import static org.pidome.platform.module.rfxcom.presentations.GenericInfoPresentation.RECEIVER_TYPES;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.handlers.AbstractProtocolHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.modules.devices.DeviceCommandRequest;
import org.pidome.platform.modules.devices.UnsupportedDeviceCommandException;
import org.pidome.platform.presentation.components.list.DescriptionList;
import org.pidome.platform.presentation.components.list.DescriptionListItem;
import org.pidome.tools.utilities.Serialization;

/**
 * Handler for RFXCom interface protocol.
 *
 * @author johns
 */
@SuppressWarnings("checkstyle:magicnumber")
public class RFXComHandler extends AbstractProtocolHandler {

    /**
     * Promise indicating if the rfxcom has been started or not.
     */
    private boolean rfxComStarted;

    /**
     * RFXCom information.
     *
     * Only available after a status request. Otherwise always empty.
     *
     */
    private GenericInfoPresentation rfxComInfo;

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(RFXComHandler.class);

    /**
     * Constructor for received data.
     *
     * @param rfxComActive If the RFXCom has been started or not (all init data
     * is send).
     * @param module The module.
     * @param parser The parser.
     */
    public RFXComHandler(final boolean rfxComActive, final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
        this.rfxComStarted = rfxComActive;
    }

    /**
     * Handle received data created by the received constructor.
     *
     * @throws PacketProtocolException
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {
        switch (this.getParser().getPacketType()) {
            case RFXCOM_IFACE:
                switch (InterfaceProtocol.byByte(this.getParser().getSubType())) {
                    case RFXCOM_MODE:
                        switch (this.getParser().getMessageBody()[0]) {
                            case 0x02:
                                LOG.info("Got status response: {}", BasicPacketParser.decodeAllBytes(this.getParser().getMessageBody()));
                                this.rfxComInfo = RFXComHandler.rfxComInfoDecoder(this.getParser());
                                if (LOG.isDebugEnabled()) {
                                    try {
                                        LOG.debug("RFXCom info [{}]", Serialization.getDefaultObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this.rfxComInfo));
                                    } catch (JsonProcessingException ex) {
                                        LOG.error("Could not write RFXCom info as prettyfied string.");
                                    }
                                }
                                /// If we have a firmware version which requires a start, we send it now (well, after 500 ms).
                                if (getParser().getAbsoluteLength() > 12) {
                                    sendToRFXCom(new RFXComSendCommand(InterfaceBaseCommands.RFXCOM_START, 500));
                                }
                                break;

                            default:
                                LOG.error("Received an unknown RFXCom interface mode command (2): {}", BasicPacketParser.decodeAllBytes(this.getParser().getMessageBody()));
                                break;
                        }
                        break;
                    case RFXCOM_START:
                        /// Check for copyright message, we then are able to say we can start.
                        LOG.info("handled by RFXCOM_START: [{}]", new String(this.getParser().getMessageBody()));
                        this.rfxComStarted = true;
                        break;
                    case RFXCOM_UNKNOWN:
                        /// Old versions need this, as they do not understand InterfaceBaseCommands.RFXCOM_START so responses on START come back as unknown.
                        /// Because it is the very first unknown type we recieve in the startup sequence, and so forth, we can reliable implement this as such.
                        if (!this.rfxComStarted) {
                            this.rfxComStarted = true;
                        } else {
                            LOG.warn("Implmentation notification, keep a copy of your appLog.txt if ecountering problems (type: {}, sub: {}): {}", this.getParser().getPacketTypeByteChar(), this.getParser().getPacketSubTypeByteChar(), BasicPacketParser.decodeAllBytes(this.getParser().getMessageBody()));
                        }
                        break;
                    default:
                        LOG.error("Invalid/Unimplmented RFXCom iface message (subtype: {}): {}", this.getParser().getPacketSubTypeByteChar(), BasicPacketParser.decodeAllBytes(this.getParser().getMessageBody()));
                        break;
                }
                break;
            default:
                LOG.error("RFXCom control type called for a non iface type packet (type): {}", BasicPacketParser.decodeAllBytes(this.getParser().getMessageBody()));
                break;
        }
    }

    /**
     * Returns true when the last message indicates the rfxcom has started.
     *
     * By default this always returns false. Only at that one moment a startup
     * sequence has completed it returns true.
     *
     * @return true when started.
     */
    public boolean startupDone() {
        return rfxComStarted;
    }

    /**
     * When a status command has been send this is set to true.
     *
     * @return True when waiting for status.
     */
    public boolean hasRFXComInfo() {
        return this.rfxComInfo != null;
    }

    /**
     * Returns the generic RFXCom info.
     *
     * Only available after a status response. You must check
     * <code>hasRFXComInfo()</code> first!
     *
     * @return Presentation on status, otherwise null.
     */
    public GenericInfoPresentation getRFXComInfo() {
        return this.rfxComInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Future<Void> handleReceivedFromDevice(final DeviceCommandRequest request, final Promise<Void> promise) {
        LOG.warn("The interface handler does not accept device commands [{}]", request);
        promise.fail(new UnsupportedDeviceCommandException("The interface protocol does not accept device commands"));
        return promise.future();
    }

    /**
     * Generates an information presentation object for showing the RFXCom
     * specifics.
     *
     * @param parser The packet parser.
     * @return The configuration based on the info message.
     */
    @SuppressWarnings("checkstyle:AvoidInlineConditionals")
    public static GenericInfoPresentation rfxComInfoDecoder(final BasicPacketParser parser) {
        GenericInfoPresentation info = new GenericInfoPresentation();
        DescriptionList deviceInfo = new DescriptionList("Device info", "Information about the device.");
        DescriptionList protocolInfo = new DescriptionList("Protocol info", "Information about active protocols.");

        int firmwareVersion;
        int firmwareType;
        int transmitterPower = 10;

        byte[] infoArray;

        if (parser.getMessageBody().length > 12) {
            infoArray = new byte[17];
            System.arraycopy(parser.getMessageBody(), 0, infoArray, 0, 17);
            firmwareVersion = infoArray[2] + 1000;
            firmwareType = infoArray[10];
            transmitterPower = infoArray[9] - 18;
        } else {
            infoArray = new byte[10];
            System.arraycopy(parser.getMessageBody(), 0, infoArray, 0, 10);

            firmwareVersion = infoArray[2];
            if (infoArray[1] == 0x52 && firmwareVersion < 162) {
                firmwareType = 0;
            } else if (infoArray[1] == 0x53 && firmwareVersion < 162) {
                firmwareType = 1;
                if (firmwareVersion >= 89) {
                    transmitterPower = infoArray[9] - 18;
                }
            } else if (infoArray[1] == 0x53 && firmwareVersion >= 162 && firmwareVersion < 225) {
                firmwareType = 2;
                if (firmwareVersion >= 189) {
                    transmitterPower = infoArray[9] - 18;
                }
            } else {
                firmwareType = 3;
                if (firmwareVersion >= 243) {
                    transmitterPower = infoArray[9] - 18;
                }
            }

        }
        if (firmwareType != 0) {
            transmitterPower = (infoArray[1] == 0x52) ? -99 : transmitterPower;
            if (transmitterPower > 13) {
                transmitterPower = 10;
            }
        } else {
            transmitterPower = 0;
        }

        deviceInfo.addListItem(new DescriptionListItem("Device type", RECEIVER_TYPES.getOrDefault((int) infoArray[1], "Unknown")));
        deviceInfo.addListItem(new DescriptionListItem("Hardware version", infoArray[7] + "." + infoArray[8] + ", " + FIRMWARE_TYPES.getOrDefault(firmwareType, "Unknown")));
        deviceInfo.addListItem(new DescriptionListItem("Firmware version", firmwareVersion));

        if (firmwareType != 0) {
            deviceInfo.addListItem(new DescriptionListItem("Transmitter power", transmitterPower + " dBm"));
        }
        if (firmwareType >= 5) {
            deviceInfo.addListItem(new DescriptionListItem("Noise", infoArray[11] + " dBm"));
        }
        info.setSections(deviceInfo, protocolInfo);

        List<String> activeProtocols = new ArrayList<>();

        for (RFXComProtocol proto : RFXComProtocol.values()) {
            if ((infoArray[proto.getBitPos()] & proto.getBit()) > 0) {
                protocolInfo.addListItem(new DescriptionListItem(proto.getName(), true));
                activeProtocols.add(proto.getName());
            } else {
                protocolInfo.addListItem(new DescriptionListItem(proto.getName(), false));
            }
        }

        LOG.info("RFXCom type: [{}], Version: [{}, {}], firmware: [{}], transmit: [{}], noise: [{}], protocols: {}",
                RECEIVER_TYPES.getOrDefault((int) infoArray[1], "Unknown"),
                infoArray[7] + "." + infoArray[8],
                FIRMWARE_TYPES.getOrDefault(firmwareType, "Unknown"),
                firmwareVersion,
                transmitterPower,
                (firmwareType >= 5) ? infoArray[11] + " dBm" : "Not implemented",
                activeProtocols
        );

        return info;
    }

}
