/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.lighting5;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.RFXComSendCommand;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.protocol.handlers.AbstractProtocolHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceCommandRequest;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DeviceStringAddress;
import org.pidome.platform.modules.devices.UnsupportedDeviceCommandException;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.presentation.components.list.DescriptionListItem;
import org.pidome.tools.utilities.MathUtil;

/**
 * Parser for lighting5 devices.
 *
 * Currently only lightwave RF devices.
 *
 * @author John
 */
@SuppressWarnings("checkstyle:magicnumber")
public final class Lighting5Handler extends AbstractProtocolHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(Lighting5Handler.class);

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType implements ProtocolDefinition {
        /**
         * Siemens lightwave.
         */
        LIGHTWAVERF(0, "Lightwave RF"),
        /**
         * An unknown type.
         */
        UNKNOWN(-1, "Unknown or unhandled sub type");
        /**
         * Supplying value.
         */
        private final int value;
        /**
         * subtype description.
         */
        private final String desc;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param bitValue Type value.
         * @param description Type description.
         */
        PacketSubType(final int bitValue, final String description) {
            this.value = bitValue;
            this.desc = description;
        }

        /**
         * @return Teh subtype value
         */
        @Override
        public int getValue() {
            return value;
        }

        /**
         * @return The subtype description.
         */
        @Override
        public String getDescription() {
            return desc;
        }

    }

    /**
     * Default group name.
     */
    public static final String DEFAULT_GROUP_NAME = "deviceactions";

    /**
     * Names of the controls used.
     */
    private enum ControlIdName {
        /**
         * One device switch.
         */
        SWITCH,
        /**
         * Grouped off.
         */
        GROUPOFF,
        /**
         * Mood 1.
         */
        MOODSELECT1,
        /**
         * Mood 2.
         */
        MOODSELECT2,
        /**
         * Mood 3.
         */
        MOODSELECT3,
        /**
         * Mood 4.
         */
        MOODSELECT4,
        /**
         * Mood 5.
         */
        MOODSELECT5,
        /**
         * Lock switch.
         */
        LOCKSWITCH,
        /**
         * All lock.
         */
        ALLLOCK,
        /**
         * Relay switch.
         */
        RELAYSWITCH,
        /**
         * Relay stop.
         */
        RELAYSTOP,
        /**
         * Level.
         */
        LEVEL,
        /**
         * Color next.
         */
        COLORNEXT,
        /**
         * Color tone.
         */
        COLORTONE,
        /**
         * Color cycle.
         */
        COLORCYCLE
    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public Lighting5Handler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {
        final PacketSubType subType = getSubPacketTypeByBitId(getParser().getSubType(), PacketSubType.UNKNOWN, PacketSubType.values());
        final byte[] body = this.getParser().getMessageBody();
        String[] deviceId = new String[3];
        for (int i = 0; i < 3; i++) {
            deviceId[i] = BasicPacketParser.decodeSingleByte(body[i]);
        }
        String baseAddress = BasicPacketParser.decodeSingleByte(body[3]);
        String address = new StringBuilder(String.join(",", deviceId)).append(":").append(baseAddress).toString();
        boolean found = false;
        for (DeviceAddress deviceAddress : this.getRunningDeviceAddresses(Lighting5Device.class)) {
            if (deviceAddress.getAddress().equals(address)) {
                found = true;
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Setting data for subtype: {} command: {}", subType, BasicPacketParser.decodeSingleByte(body[4]));
                }
                try {
                    switch (subType) {
                        case LIGHTWAVERF:
                            DeviceDataNotification notification = new DeviceDataNotification(deviceAddress);
                            switch (body[4]) {
                                case 0x00:
                                case 0x01:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.SWITCH.name(), body[4] == 0x01);
                                    break;
                                case 0x02:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.GROUPOFF.name(), true);
                                    break;
                                case 0x03:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.MOODSELECT1.name(), true);
                                    break;
                                case 0x04:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.MOODSELECT2.name(), true);
                                    break;
                                case 0x05:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.MOODSELECT3.name(), true);
                                    break;
                                case 0x06:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.MOODSELECT4.name(), true);
                                    break;
                                case 0x07:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.MOODSELECT5.name(), true);
                                    break;
                                case 0x0A:
                                case 0x0B:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.LOCKSWITCH.name(), body[4] == 0x0B);
                                    break;
                                case 0x0C:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.ALLLOCK.name(), true);
                                    break;
                                case 0x0D:
                                case 0x0F:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.RELAYSWITCH.name(), body[4] == 0x0F);
                                    break;
                                case 0x0E:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.RELAYSTOP.name(), true);
                                    break;
                                case 0x10:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.LEVEL.name(),
                                            createReversedDimLevel(Integer.parseInt(BasicPacketParser.decodeSingleByte(body[5]), 16))
                                    );
                                    break;
                                case 0x11:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.COLORNEXT.name(), true);
                                    break;
                                case 0x12:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.COLORTONE.name(), true);
                                    break;
                                case 0x13:
                                    notification.addData(DEFAULT_GROUP_NAME, ControlIdName.COLORCYCLE.name(), true);
                                    break;
                                default:
                                    throw new UnsupportedDeviceCommandException("Unsupported command: " + BasicPacketParser.decodeSingleByte(body[4]));
                            }
                            this.sendDataNotification(notification);
                            break;
                        default:
                            throw new UnsupportedDeviceCommandException("Unsupported protocol bit: " + subType);
                    }
                } catch (UnsupportedDeviceCommandException ex) {
                    LOG.error("Unknown device command [{}]", ex.getMessage());
                }
            }
        }
        if (!found && this.discoveryEnabled()) {

            DiscoveredDevice<DeviceStringAddress> newDevice = new DiscoveredDevice<>(Lighting5Device.class, "Lightwave RF device");

            newDevice.addDescriptionListItem(new DescriptionListItem("Possible types", "Lightwave RF"));
            newDevice.addDescriptionListItem(new DescriptionListItem("Remote address", baseAddress));
            newDevice.addDescriptionListItem(new DescriptionListItem("packettype", ProtocolCollection.PacketType.LIGHTING_5.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("packetsubtype", subType.toString()));

            newDevice.addFieldParameter("packettype", ProtocolCollection.PacketType.LIGHTING_5.toString());
            newDevice.addFieldParameter("packetsubtype", subType.toString());

            DeviceStringAddress newAddress = new DeviceStringAddress();
            newAddress.setAddress(baseAddress);
            newDevice.setAddress(newAddress);

            this.discovered(newDevice);

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Future<Void> handleReceivedFromDevice(final DeviceCommandRequest request, final Promise<Void> promise) {
        LOG.info("Request done: [{}]", request);
        request.getDevice().getParameters().get("packetsubtype").ifPresentOrElse(parameter -> {
            try {
                byte level = (byte) 0x00;
                byte command = (byte) 0x00;
                switch (ControlIdName.valueOf(request.getControlId())) {
                    case SWITCH:
                        if ((boolean) request.getCommandValue()) {
                            command = (byte) 0x01;
                        } else {
                            command = (byte) 0x00;
                        }
                        break;
                    case GROUPOFF:
                        command = (byte) 0x02;
                        break;
                    case MOODSELECT1:
                        command = (byte) 0x03;
                        break;
                    case MOODSELECT2:
                        command = (byte) 0x04;
                        break;
                    case MOODSELECT3:
                        command = (byte) 0x05;
                        break;
                    case MOODSELECT4:
                        command = (byte) 0x06;
                        break;
                    case MOODSELECT5:
                        command = (byte) 0x07;
                        break;
                    case LOCKSWITCH:
                        if ((boolean) request.getCommandValue()) {
                            command = (byte) 0x0B;
                        } else {
                            command = (byte) 0x0A;
                        }
                        break;
                    case ALLLOCK:
                        command = (byte) 0x0C;
                        break;
                    case RELAYSWITCH:
                        if ((boolean) request.getCommandValue()) {
                            command = (byte) 0x0F;
                        } else {
                            command = (byte) 0x0D;
                        }
                        break;
                    case RELAYSTOP:
                        command = (byte) 0x0E;
                        break;
                    case LEVEL:
                        command = (byte) 0x10;
                        level = createDimLevel(Integer.parseInt(request.getCommandValue().toString()));
                        break;
                    case COLORNEXT:
                        command = (byte) 0x11;
                        break;
                    case COLORTONE:
                        command = (byte) 0x12;
                        break;
                    case COLORCYCLE:
                        command = (byte) 0x13;
                        break;
                    default:
                        throw new UnsupportedDeviceCommandException("Unknown control id " + request.getControlId());
                }
                String[] idLocation = ((String) request.getDevice().getAddress().getAddress()).split(":");
                byte unitCode = (byte) Integer.parseInt(idLocation[1], 16);
                String[] id = idLocation[0].split(",");
                byte[] idSet = new byte[3];
                for (int i = 0; i < id.length; i++) {
                    idSet[i] = (byte) Integer.parseInt(id[i], 16);
                }
                byte[] toSend = new byte[]{
                    (byte) 0x0A,
                    (byte) 0x14,
                    (byte) getSubPacketTypeIntByStringId(parameter.getValue(), PacketSubType.UNKNOWN, PacketSubType.values()),
                    (byte) 0x01,
                    idSet[0],
                    idSet[1],
                    idSet[2],
                    unitCode,
                    command,
                    level,
                    (byte) 0x00
                };
                RFXComSendCommand rfxSend = new RFXComSendCommand(toSend, 0);
                this.sendToRFXCom(rfxSend);
                promise.complete();
            } catch (PacketProtocolException ex) {
                LOG.error("Error in protocol handling", ex);
                promise.fail(ex);
            } catch (UnsupportedDeviceCommandException ex) {
                LOG.error("Error in device command handling handling", ex);
                promise.fail(ex);
            }
        }, () -> {
            promise.fail(new Exception("No subtype present to identify type with: " + request.getDevice().getParameters().get("packetsubtype")));
        });
        return promise.future();
    }

    /**
     * Creates the dim level (0-100 -> 1 - 31).
     *
     * From percentage to data.
     *
     * @param percentage The percentage.
     * @return The byte.
     * @throws PacketProtocolException outside percentage values.
     */
    private byte createDimLevel(final int percentage) throws PacketProtocolException {
        if (percentage > 100 || percentage < 0) {
            throw new PacketProtocolException("Impossible dim value: " + percentage);
        }
        return (byte) ((int) MathUtil.map(percentage, 0, 100, 0, 31));
    }

    /**
     * Creates the reversed dim level (1 - 31 -> 0 - 100).
     *
     * From data to percentage.
     *
     * @param bytes The recieved data.
     * @return The percentage.
     * @throws PacketProtocolException When outside the byte value.
     */
    private int createReversedDimLevel(final int bytes) throws PacketProtocolException {
        if (bytes < 0 || bytes > 31) {
            throw new PacketProtocolException("Impossible dim value: " + bytes);
        }
        return ((int) MathUtil.map(bytes, 0, 31, 0, 100));
    }

}
