/**
 * Handlers and devices for combined temperature, humidity and pressure based devices.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.temperaturehumiditybarometer;
