/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.modules.devices.DeviceCommandRequest;

/**
 * Handler for unused protocols.
 *
 * @author John
 */
public class UnusedProtocolHandler extends AbstractProtocolHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(UnusedProtocolHandler.class);

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public UnusedProtocolHandler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param request The request done by a device.
     */
    public UnusedProtocolHandler(final PidomeRFXCom module, final DeviceCommandRequest request) {
        super(module, request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {
        LOG.warn("Unhandled protocol [{}], [{}]. Body: [{}]",
                this.getParser().getPacketTypeByteChar(),
                this.getParser().getPacketSubTypeByteChar(),
                BasicPacketParser.decodeAllBytes(this.getParser().getMessageBody()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Future<Void> handleReceivedFromDevice(final DeviceCommandRequest command, final Promise<Void> promise) {
        LOG.warn("Unhandled device command [{}]",
                command.toString());
        promise.complete();
        return promise.future();
    }

}
