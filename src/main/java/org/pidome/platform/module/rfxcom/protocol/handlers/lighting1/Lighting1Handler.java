/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.lighting1;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.RFXComSendCommand;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.protocol.handlers.AbstractProtocolHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceCommandRequest;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DeviceStringAddress;
import org.pidome.platform.modules.devices.UnsupportedDeviceCommandException;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.presentation.components.list.DescriptionListItem;

/**
 * Handler for the lighting protocol.
 *
 * @author johns
 */
@SuppressWarnings("checkstyle:magicnumber")
public final class Lighting1Handler extends AbstractProtocolHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(Lighting1Handler.class);

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType implements ProtocolDefinition {
        /**
         * AC protocol.
         */
        X10(0, "X10"),
        /**
         * HA_EU.
         */
        ARC(1, "Adress wheels: KlikAanKlikUit, Anslut, Chacon, CoCo, DI.O, HomeEasy (UK), Intertechno, NEXA"),
        /**
         * Elro AB400D.
         */
        ELRO_AB400D(2, "Flamingo, AB400, Impuls, Sartano, Brennenstuhl"),
        /**
         * Waveman.
         */
        WAVEMAN(3, "Waveman"),
        /**
         * EMW200.
         */
        EMW200(4, "EMW 200"),
        /**
         * Impuls.
         */
        IMPULS(5, "Impuls"),
        /**
         * RisingSun.
         */
        RISING_SUN(6, "Rising Sun"),
        /**
         * Philips SBC.
         */
        PHILIPS_SBC(7, "Philips SBC"),
        /**
         * EnerGenie.
         */
        ENERGENIE(8, "Energenie"),
        /**
         * Energenie5.
         */
        ENERGENIE5(9, "EnerGenie5"),
        /**
         * COCO GDR2.
         */
        COCO_GDR2(10, "CoCo GDR2"),
        /**
         * An unknown subtype.
         */
        UNKNOWN(-1, "Unknown type");
        /**
         * Supplying value.
         */
        private final int value;
        /**
         * The description of the subtype.
         */
        private final String desc;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param bitValue Type value.
         * @param description Type description.
         */
        PacketSubType(final int bitValue, final String description) {
            this.value = bitValue;
            this.desc = description;
        }

        /**
         * @return The subtype value.
         */
        @Override
        public int getValue() {
            return value;
        }

        /**
         * @return The description of the type.
         */
        @Override
        public String getDescription() {
            return desc;
        }

    }

    /**
     * This device has one group.
     */
    public static final String GENERIC_DEVICE_GROUP = "deviceactions";

    /**
     * Controls names in the groups are fixed.
     */
    public enum ControlName {
        /**
         * Generic switch.
         */
        SWITCH,
        /**
         * Generic group switch.
         */
        GROUPSWITCH,
        /**
         * Dim.
         */
        DIM,
        /**
         * Bright.
         */
        BRIGHT,
        /**
         * Chime.
         */
        CHIME
    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public Lighting1Handler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {
        PacketSubType subType = getSubPacketTypeByBitId(getParser().getSubType(), PacketSubType.UNKNOWN, PacketSubType.values());
        byte[] bytes = this.getParser().getMessageBody();
        String address = new StringBuilder(BasicPacketParser.decodeSingleByte(bytes[1])).append(":").append(BasicPacketParser.decodeSingleByte(bytes[2])).toString();
        boolean found = false;
        for (DeviceAddress deviceAddress : this.getRunningDeviceAddresses(Lighting1Device.class)) {
            if (deviceAddress.getAddress().equals(address)) {
                found = true;
                DeviceDataNotification notification = new DeviceDataNotification(deviceAddress);
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Setting data for command type: {}", BasicPacketParser.decodeSingleByte(bytes[3]));
                }
                try {
                    switch (bytes[3]) {
                        case 0x00:
                            notification.addData(GENERIC_DEVICE_GROUP, ControlName.SWITCH.name(), false);
                            break;
                        case 0x01:
                            notification.addData(GENERIC_DEVICE_GROUP, ControlName.SWITCH.name(), true);
                            break;
                        case 0x02:
                            notification.addData(GENERIC_DEVICE_GROUP, ControlName.DIM.name(), true);
                            break;
                        case 0x03:
                            notification.addData(GENERIC_DEVICE_GROUP, ControlName.DIM.name(), false);
                            break;
                        case 0x05:
                            notification.addData(GENERIC_DEVICE_GROUP, ControlName.GROUPSWITCH.name(), false);
                            break;
                        case 0x06:
                            notification.addData(GENERIC_DEVICE_GROUP, ControlName.GROUPSWITCH.name(), true);
                            break;
                        case 0x07:
                            notification.addData(GENERIC_DEVICE_GROUP, ControlName.CHIME.name(), true);
                            break;
                        default:
                            throw new UnsupportedDeviceCommandException("Unsupported command: " + BasicPacketParser.decodeSingleByte(bytes[3]));
                    }
                    this.sendDataNotification(notification);
                } catch (UnsupportedDeviceCommandException ex) {
                    LOG.error("Unknown device command [{}]", ex.getMessage());
                }
                break;
            }
        }
        if (!found && this.discoveryEnabled()) {

            DiscoveredDevice<DeviceStringAddress> newDevice = new DiscoveredDevice<>(Lighting1Device.class, new StringBuilder(subType.getDescription()).append(" or compatible").toString());
            DeviceStringAddress addr = new DeviceStringAddress();
            addr.setAddress(address);

            newDevice.addDescriptionListItem(new DescriptionListItem("Remote address", address));
            newDevice.addDescriptionListItem(new DescriptionListItem("Provides", "On/Off,Dimming,etc.. depending on the device discovered"));
            newDevice.addDescriptionListItem(new DescriptionListItem("packettype", ProtocolCollection.PacketType.LIGHTING_1.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("packetsubtype", subType.toString()));

            newDevice.addFieldParameter("packettype", ProtocolCollection.PacketType.LIGHTING_1.toString());
            newDevice.addFieldParameter("packetsubtype", subType.toString());
            this.discovered(newDevice);
        }
    }

    /**
     * @inheritDoc
     * @return The future created from the promise.
     */
    @Override
    public Future<Void> handleReceivedFromDevice(final DeviceCommandRequest request, final Promise<Void> promise) {
        LOG.info("Request done: [{}]", request);
        request.getDevice().getParameters().get("packetsubtype").ifPresentOrElse(parameter -> {
            try {
                byte command = (byte) 0x00;
                String[] idLocation = ((String) request.getDevice().getAddress().getAddress()).split(":");

                byte houseCode = translateHouseCode(idLocation[0]);
                byte unitCode = (byte) Integer.parseInt(idLocation[1]);

                switch (ControlName.valueOf(request.getControlId())) {
                    case SWITCH:
                        if ((boolean) request.getCommandValue()) {
                            command = (byte) 0x01;
                        } else {
                            command = (byte) 0x00;
                        }
                        break;
                    case DIM:
                        command = (byte) 0x02;
                        break;
                    case BRIGHT:
                        command = (byte) 0x03;
                        break;
                    case GROUPSWITCH:
                        if ((boolean) request.getCommandValue()) {
                            command = (byte) 0x04;
                        } else {
                            command = (byte) 0x05;
                        }
                        break;
                    case CHIME:
                        if ((boolean) request.getCommandValue()) {
                            command = (byte) 0x07;
                            unitCode = (byte) 0x08;
                        }
                        break;
                    default:
                        throw new UnsupportedDeviceCommandException("Unknown control id " + request.getControlId());
                }
                byte[] toSend = new byte[]{
                    (byte) 0x07,
                    (byte) 0x10,
                    (byte) getSubPacketTypeIntByStringId(parameter.getValue(), PacketSubType.UNKNOWN, PacketSubType.values()),
                    (byte) 0x00,
                    houseCode,
                    unitCode,
                    command,
                    (byte) 0x70
                };
                RFXComSendCommand rfxSend = new RFXComSendCommand(toSend, 0);
                this.sendToRFXCom(rfxSend);
                promise.complete();
            } catch (UnsupportedDeviceCommandException ex) {
                LOG.error("Unknown control id [{}]", request.getControlId());
                promise.fail(ex);
            }
        }, () -> {
            promise.fail(new Exception("No subtype present to identify type with: " + request.getDevice().getParameters().get("packetsubtype")));
        });
        return promise.future();
    }

    /**
     * Translates the given house code to the corresponding byte.
     *
     * @param houseCode The house code supplied.
     * @return The house code byte, if an invalid house code is given A value is
     * returned.
     */
    private byte translateHouseCode(final String houseCode) {
        switch (houseCode) {
            case "A":
                return (byte) 0x41;
            case "B":
                return (byte) 0x42;
            case "C":
                return (byte) 0x43;
            case "D":
                return (byte) 0x44;
            case "E":
                return (byte) 0x45;
            case "F":
                return (byte) 0x46;
            case "G":
                return (byte) 0x47;
            case "H":
                return (byte) 0x48;
            case "I":
                return (byte) 0x49;
            case "J":
                return (byte) 0x4A;
            case "K":
                return (byte) 0x4B;
            case "L":
                return (byte) 0x4C;
            case "M":
                return (byte) 0x4D;
            case "N":
                return (byte) 0x4E;
            case "O":
                return (byte) 0x4F;
            case "P":
                return (byte) 0x50;
            default:
                return (byte) 0x41;
        }
    }
}
