/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol;

import org.pidome.platform.modules.devices.Device;
import org.pidome.platform.modules.devices.DeviceAddress;

/**
 * Minimal base definitions required for a RFXCOM device.
 *
 * @author John
 * @param <T> The address type.
 */
public abstract class AbstractRFXComDevice<T extends DeviceAddress> extends Device<T> {

    /**
     * @return Returns the packet sub type.
     * @throws PacketProtocolException On unknown subtype.
     */
    public abstract int getPacketSubType() throws PacketProtocolException;

    /**
     * @return The packet type.
     */
    public abstract ProtocolCollection.PacketType getPacketType();

}
