/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.protocol.handlers.AbstractProtocolHandler;
import org.pidome.platform.module.rfxcom.protocol.handlers.UnusedProtocolHandler;
import org.pidome.platform.module.rfxcom.protocol.handlers.humidity.HumidityHandler;
import org.pidome.platform.module.rfxcom.protocol.handlers.lighting1.Lighting1Handler;
import org.pidome.platform.module.rfxcom.protocol.handlers.lighting2.Lighting2Handler;
import org.pidome.platform.module.rfxcom.protocol.handlers.lighting5.Lighting5Handler;
import org.pidome.platform.module.rfxcom.protocol.handlers.rain.RainHandler;
import org.pidome.platform.module.rfxcom.protocol.handlers.rfxcom.RFXComHandler;
import org.pidome.platform.module.rfxcom.protocol.handlers.temperature.TemperatureHandler;
import org.pidome.platform.module.rfxcom.protocol.handlers.temperaturehumidity.TemperatureHumidityHandler;
import org.pidome.platform.module.rfxcom.protocol.handlers.temperaturehumiditybarometer.TemperatureHumidityBarometerHandler;
import org.pidome.platform.module.rfxcom.protocol.handlers.wind.WindHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;

/**
 * Constants used.
 *
 * @author John
 */
public final class ProtocolCollection {

    /**
     * Collection class private constructor.
     */
    private ProtocolCollection() {
        /// private, is a collection set.
    }

    /**
     * Supported protocols by this plugin.
     */
    public enum PacketType {
        /**
         * RFXCom Control message type.
         */
        RFXCOM_CTRL("Control", 0x00, UnusedProtocolHandler.class),
        /**
         * RFXCom message type.
         */
        RFXCOM_IFACE("Status/Mode", 0x01, RFXComHandler.class),
        /**
         * RFXCom transmit result.
         */
        RFXCOM_TRANS_MSG("Result", 0x02, UnusedProtocolHandler.class),
        /**
         * Identifying all lighting protocol types.
         */
        LIGHTING_1("Lighting 1", 0x10, Lighting1Handler.class),
        /**
         * Identifying all lighting protocol types.
         */
        LIGHTING_2("Lighting 2", 0x11, Lighting2Handler.class),
        /**
         * Identifying all lighting protocol types.
         */
        LIGHTING_5("Lighting 5", 0x14, Lighting5Handler.class),
        /**
         * Oregon 0x50 based.
         */
        TEMPERATURE("Temperature", 0x50, TemperatureHandler.class),
        /**
         * Oregon 0x51 based.
         */
        HUMIDITY("Humidity", 0x51, HumidityHandler.class),
        /**
         * Oregon 0x52 based.
         */
        TEMPERATURE_HUMIDITY("Temperature and humidity", 0x52, TemperatureHumidityHandler.class),
        /**
         * Oregon 0x54 based.
         */
        TEMPERATURE_HUMIDITY_BAROMETER("Temperature, humidity and pressure", 0x54, TemperatureHumidityBarometerHandler.class),
        /**
         * Oregon 0x55 based.
         */
        RAIN("Rain", 0x55, RainHandler.class),
        /**
         * Oregon 0x56 based.
         */
        WIND("Wind", 0x56, WindHandler.class);

        /**
         * Readable name.
         */
        private final String protocolName;

        /**
         * the bit to identify the type.
         */
        private final int typeBit;

        /**
         * The handler for the type.
         */
        private final Class<? extends AbstractProtocolHandler> handler;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param name The Name of the type.
         * @param bit The identifying bit.
         * @param handlerClass the class handling this protocol.
         */
        PacketType(final String name, final int bit, final Class<? extends AbstractProtocolHandler> handlerClass) {
            this.protocolName = name;
            this.typeBit = bit;
            this.handler = handlerClass;
        }

        /**
         * @return The protocol name.
         */
        public String getProtocolName() {
            return protocolName;
        }

        /**
         * @return The status bit.
         */
        public int getTypeBit() {
            return this.typeBit;
        }

        /**
         * Returns the handler class.
         *
         * @return The class which handles the protocol.
         */
        public Class<? extends AbstractProtocolHandler> getHandler() {
            return this.handler;
        }

    }

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType {
        /**
         * Send ok.
         */
        ACK(0),
        /**
         * Send with delay.
         */
        ACK_DELAY(1),
        /**
         * Not send.
         */
        NAK(2),
        /**
         * Not send, invalid.
         */
        NAK_INVALID(3);

        /**
         * Supplying value.
         */
        private final int value;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param subTypeVal int value for the subtype.
         */
        PacketSubType(final int subTypeVal) {
            this.value = subTypeVal;
        }

        /**
         * Returns the enum value.
         *
         * @return The value of the subtype.
         */
        public int getValue() {
            return value;
        }

    }

    /**
     * Base protocol mapper.
     */
    private static Map<Integer, PacketType> packetTypes;

    /**
     * Returns a map containing supported base protocols mapped by byte (int
     * value).
     *
     * @return Map of packet types.
     */
    public static Map<Integer, PacketType> getPacketTypes() {
        if (packetTypes == null) {
            HashMap<Integer, PacketType> typeMap = new HashMap<>(PacketType.values().length);
            for (PacketType type : PacketType.values()) {
                typeMap.put(type.getTypeBit(), type);
            }
            packetTypes = Collections.unmodifiableMap(typeMap);
        }
        return packetTypes;
    }

    /**
     * Creates a protocol handler for handling the protocol data.
     *
     * @param engine The driver reference.
     * @param parser The packet parser.
     * @return Basic packet parser as handler.
     * @throws PacketProtocolException when the handler can not be created.
     */
    public static AbstractProtocolHandler createProtocolHandler(final PidomeRFXCom engine, final BasicPacketParser parser) throws PacketProtocolException {
        Class<? extends AbstractProtocolHandler> handlerClass = parser.getPacketType().getHandler();
        try {
            return handlerClass.getConstructor(PidomeRFXCom.class, BasicPacketParser.class)
                    .newInstance(engine, parser);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new PacketProtocolException("Problem instantiating protocol handler", ex);
        }
    }

}
