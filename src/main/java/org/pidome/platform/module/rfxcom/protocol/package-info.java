/**
 * Protocol collection based classes.
 *
 * Supporting and base classes for device, protocols and definitions.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom.protocol;
