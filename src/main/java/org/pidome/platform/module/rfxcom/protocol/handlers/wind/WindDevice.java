/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.wind;

import org.pidome.platform.module.rfxcom.protocol.AbstractRFXComDevice;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.modules.devices.DeviceStringAddress;

/**
 * Device used for wind information.
 *
 * @author John
 */
public class WindDevice extends AbstractRFXComDevice<DeviceStringAddress> {

    /**
     * The subType.
     */
    private WindHandler.PacketSubType subType = WindHandler.PacketSubType.TYPE1;

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPacketSubType() {
        return subType.getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProtocolCollection.PacketType getPacketType() {
        return ProtocolCollection.PacketType.WIND;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void shutdownDevice() {
        /// not used.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startupDevice() {
        this.getParameters().get("subtype").ifPresentOrElse(parameter -> {
            this.subType = WindHandler.PacketSubType.valueOf(parameter.getValue());
        }, () -> {
            this.subType = WindHandler.PacketSubType.UNKNOWN;
        });
    }

}
