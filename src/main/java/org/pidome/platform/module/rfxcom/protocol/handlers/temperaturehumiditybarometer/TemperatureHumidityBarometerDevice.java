/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.temperaturehumiditybarometer;

import org.pidome.platform.module.rfxcom.protocol.AbstractRFXComDevice;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.modules.devices.DeviceStringAddress;

/**
 * Device for temperature, humidity and pressure.
 *
 * @author John
 */
public class TemperatureHumidityBarometerDevice extends AbstractRFXComDevice<DeviceStringAddress> {

    /**
     * The subtype.
     */
    private TemperatureHumidityBarometerHandler.PacketSubType subType = TemperatureHumidityBarometerHandler.PacketSubType.TYPE1;

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPacketSubType() {
        return subType.getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProtocolCollection.PacketType getPacketType() {
        return ProtocolCollection.PacketType.TEMPERATURE_HUMIDITY_BAROMETER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void shutdownDevice() {
        /// not used.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startupDevice() {
        this.getParameters().get("subtype").ifPresentOrElse(parameter -> {
            this.subType = TemperatureHumidityBarometerHandler.PacketSubType.valueOf(parameter.getValue());
        }, () -> {
            this.subType = TemperatureHumidityBarometerHandler.PacketSubType.UNKNOWN;
        });
    }

}
