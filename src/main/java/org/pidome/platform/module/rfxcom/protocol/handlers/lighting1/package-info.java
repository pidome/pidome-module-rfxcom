/**
 * Handlers and devices for Lighting1 based devices.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.lighting1;
