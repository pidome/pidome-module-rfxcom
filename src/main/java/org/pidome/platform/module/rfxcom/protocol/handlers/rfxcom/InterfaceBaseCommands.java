/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.rfxcom;

/**
 * Generic fixed commands to be send to the RFXCom.
 *
 * @author johns
 */
public final class InterfaceBaseCommands {

    /**
     * Data container, private constructor.
     */
    private InterfaceBaseCommands() {
        /// Private constructor.
    }

    /**
     * Reset command for the RFXCom.
     */
    public static final byte[] RFXCOM_RESET = {0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    /**
     * To request a status from the RFXCom.
     */
    public static final byte[] RFXCOM_STATUS = {0x0D, 0x00, 0x00, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    /**
     * To have the RFXCom start.
     */
    public static final byte[] RFXCOM_START = {0x0D, 0x00, 0x00, 0x02, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

}
