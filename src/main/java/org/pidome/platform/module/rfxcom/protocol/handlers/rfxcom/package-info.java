/**
 * Handlers and devices for handling the RFXCom it self.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.rfxcom;
