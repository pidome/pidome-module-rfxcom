/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.modules.devices.DeviceCommandRequest;
import org.pidome.platform.modules.devices.UnsupportedDeviceCommandException;

/**
 * A handler wich only receives data from the RFXCom.
 *
 * @author johns
 */
public abstract class DataOnlyHandler extends AbstractProtocolHandler {

    /**
     * Instance logger.
     */
    private final Logger instanceLogger = LogManager.getLogger(this.getClass().getName());

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public DataOnlyHandler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Future<Void> handleReceivedFromDevice(final DeviceCommandRequest command, final Promise<Void> promise) {
        instanceLogger.error("This device does not accept commands [{}]", command);
        promise.fail(new UnsupportedDeviceCommandException("Device does not accept commands"));
        return promise.future();
    }

}
