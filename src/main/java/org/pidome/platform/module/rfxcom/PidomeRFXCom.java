/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.hardware.driver.types.consumers.SerialDataConsumer;
import org.pidome.platform.hardware.driver.types.consumers.SerialDataProsumer;
import org.pidome.platform.module.rfxcom.presentations.GenericInfoPresentation;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection.PacketType;
import org.pidome.platform.module.rfxcom.protocol.handlers.AbstractProtocolHandler;
import org.pidome.platform.module.rfxcom.protocol.handlers.rfxcom.InterfaceBaseCommands;
import org.pidome.platform.module.rfxcom.protocol.handlers.rfxcom.RFXComHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParserException;
import org.pidome.platform.modules.ModuleTargets;
import org.pidome.platform.modules.PidomeModule;
import org.pidome.platform.modules.devices.DeviceCommandRequest;
import org.pidome.platform.modules.devices.DevicesModule;
import org.pidome.platform.presentation.Presentation;
import org.pidome.platform.presentation.input.InputForm;

/**
 * Module for RFXCom communications and devices.
 *
 * @author johns
 */
@PidomeModule(
        name = "PiDome RFXCom USB",
        description = "PiDome implementation for the RFXCom peripheral",
        transport = Transport.SubSystem.SERIAL
)
@ModuleTargets(
        targets = {"VID_0403&PID_6015"},
        exclusive = true
)
@SuppressWarnings("checkstyle:magicnumber")
public final class PidomeRFXCom extends DevicesModule<SerialDataProsumer> {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(PidomeRFXCom.class);

    /**
     * In case of required delayed data send, use this executor.
     */
    private final ScheduledExecutorService delayedDataSend = Executors.newSingleThreadScheduledExecutor();

    /**
     * Consumer of data from the driver.
     */
    private final SerialDataConsumer handleDataFromDriver = this::handleDataFromDriver;

    /**
     * The start promise to complete or fail after full init. PiDome now fully
     * starts Async so we are able to complete after the correct messages have
     * been received.
     */
    private Promise driverStartCompletedPromise;

    /**
     * Generic information about the connected RFXCom.
     */
    private GenericInfoPresentation rfxComInfo;

    /**
     * Starts the RFXCom module.
     *
     * @param startPromise Promise to succeed or fail.
     */
    @Override
    public void startModule(final Promise startPromise) {
        this.driverStartCompletedPromise = startPromise;
        /// It can take a while before all is done, just supply an empty info set.
        rfxComInfo = new GenericInfoPresentation();
        this.getProsumer().setDriverConsumer(handleDataFromDriver);
        /// First call, builds the library.
        ProtocolCollection.getPacketTypes();
        LOG.info("Sending RFXCom reset after 500 ms");
        sendToRFXCom(new RFXComSendCommand(InterfaceBaseCommands.RFXCOM_RESET, 500));
        LOG.info("Sending RFXCom status 3000 ms after status");
        sendToRFXCom(new RFXComSendCommand(InterfaceBaseCommands.RFXCOM_STATUS, 3500));
    }

    /**
     * Returns the capabilities of the module which extends the default
     * functionalities.
     *
     * @return This modules capabilities.
     */
    @Override
    public Capabilities[] capabilities() {
        return new Capabilities[]{Capabilities.DISCOVERY};
    }

    /**
     * Returns information about the RFXCom.
     *
     * @return The information about the RFXCom.
     */
    @Override
    public final Presentation getModuleInfo() {
        return this.rfxComInfo;
    }

    /**
     * Stops the RFXCom module.
     *
     * @param stopPromise Promise to succeed or fail.
     */
    @Override
    public void stopModule(final Promise stopPromise) {
        this.getProsumer().setDriverConsumer(null);
        stopPromise.complete();
    }

    /**
     * Sends data to the RFXCom.
     *
     * @param sendCommand The command to send.
     */
    public void sendToRFXCom(final RFXComSendCommand sendCommand) {
        delayedDataSend.schedule(() -> {
            getProsumer().produceByModule(sendCommand.getData());
        }, sendCommand.getDelayed(), TimeUnit.MILLISECONDS);
    }

    /**
     * Handles data send from a device.
     *
     * @param command The command coming from a device.
     * @param promise The promise to succeed for success, or fail with exception
     * to fail.
     * @return Future indicating successful or failing device command execution.
     */
    @Override
    public Future<Void> handleReceivedFromDevice(final DeviceCommandRequest command, final Promise<Void> promise) {
        LOG.info(command.toString());
        promise.complete();
        return promise.future();
    }

    /**
     * Handler for handling data from the serial driver.
     *
     * @param data The data coming from the serial driver.
     */
    private void handleDataFromDriver(final byte[] data) {
        try {
            LOG.debug("Got data from RFXCom device: {}", data);
            if (LOG.isDebugEnabled()) {
                LOG.debug("RFXCom debug full receive: {}", BasicPacketParser.decodeAllBytes(data));
            }
            try {
                int length = (data[0] & 0xFF) + 1; ///Get the bytes body length + the body length byte(+1).
                byte[] workSet = Arrays.copyOfRange(data, 0, length);
                if (LOG.isDebugEnabled()) {
                    LOG.debug("RFXCom parser handle: {} (length: [{}])", BasicPacketParser.decodeAllBytes(workSet), workSet.length);
                }
                BasicPacketParser parser = new BasicPacketParser(workSet);
                try {
                    parser.decode();
                    AbstractProtocolHandler handler;
                    switch (parser.getPacketType()) {
                        case RFXCOM_IFACE:
                            handler = new RFXComHandler(driverStartCompletedPromise == null, this, parser);
                            break;
                        default:
                            handler = ProtocolCollection.createProtocolHandler(this, parser);
                    }
                    handler.handleReceivedFromDriver();
                    if (parser.getPacketType().equals(PacketType.RFXCOM_IFACE)) {
                        RFXComHandler iFaceHandler = ((RFXComHandler) handler);
                        if (iFaceHandler.hasRFXComInfo()) {
                            this.rfxComInfo = iFaceHandler.getRFXComInfo();
                        }
                        /// Need to check if a startup sequence has been completed.
                        if (driverStartCompletedPromise != null && iFaceHandler.startupDone()) {
                            driverStartCompletedPromise.complete();
                            driverStartCompletedPromise = null;
                        }
                    }
                } catch (NullPointerException ex) {
                    LOG.error("Packet parsed, but there is unknown information: {}, raw: {}", ex.getMessage(), parser.toString(), ex);
                } catch (BasicPacketParserException ex) {
                    LOG.error("Problem parsing packet into basic structure: {}, raw: {}", ex.getMessage(), parser.toString(), ex);
                } catch (PacketProtocolException ex) {
                    LOG.error("Protocol/handler error: {}. Raw: {}", ex.getMessage(), parser.toString(), ex);
                }
            } catch (ArrayIndexOutOfBoundsException ex) {
                //// We are att the end of the array;
                LOG.trace("Reached end of byte array to soon: {}", BasicPacketParser.decodeAllBytes(data), ex);
            }
        } catch (Exception ex) {
            LOG.error("Unhandled exception in driver: {}", ex.getMessage(), ex);
        }
    }

    /**
     * Called when the server requests a configuration to be created.
     *
     * @param prms promise to complete or fail.
     */
    @Override
    public void composeConfiguration(final Promise prms) {
        prms.complete();
    }

    /**
     * Called when the server posts the configuration with user values.
     *
     * @param t The input form.
     * @param prms promise to complete or fail.
     */
    @Override
    public void configure(final InputForm t, final Promise prms) {
        prms.complete();
    }
}
