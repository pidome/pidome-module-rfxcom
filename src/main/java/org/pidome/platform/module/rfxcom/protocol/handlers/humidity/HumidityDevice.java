/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.humidity;

import org.pidome.platform.module.rfxcom.protocol.AbstractRFXComDevice;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.modules.devices.DeviceStringAddress;

/**
 * Device defining an humidity device.
 *
 * @author John
 */
public class HumidityDevice extends AbstractRFXComDevice<DeviceStringAddress> {

    /**
     * The subtype of this device.
     */
    private HumidityHandler.PacketSubType subtype = HumidityHandler.PacketSubType.UNKNOWN;

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPacketSubType() throws PacketProtocolException {
        return subtype.getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProtocolCollection.PacketType getPacketType() {
        return ProtocolCollection.PacketType.HUMIDITY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void shutdownDevice() {
        /// not used.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startupDevice() {
        this.getParameters().get("subtype").ifPresentOrElse(parameter -> {
            this.subtype = HumidityHandler.PacketSubType.valueOf(parameter.getValue());
        }, () -> {
            this.subtype = HumidityHandler.PacketSubType.UNKNOWN;
        });
    }

}
