/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.presentations;

import java.util.HashMap;
import java.util.Map;
import org.pidome.platform.presentation.Presentation;

/**
 * Presentation for generic information.
 *
 * @author johns
 */
@SuppressWarnings("checkstyle:magicnumber")
public class GenericInfoPresentation extends Presentation {

    /**
     * Receiver types.
     */
    public static final Map<Integer, String> RECEIVER_TYPES = new HashMap<Integer, String>() {
        {
            put(0x50, "310MHz");
            put(0x51, "315MHz");
            put(0x52, "433.92MHz receiver only");
            put(0x53, "433.92MHz transceiver");
            put(0x54, "433.42MHz transceiver");
            put(0x55, "868.00MHz");
            put(0x56, "868.00MHz FSK");
            put(0x57, "868.30MHz");
            put(0x58, "868.30MHz FSK");
            put(0x59, "868.35MHz");
            put(0x5A, "868.35MHz FSK");
            put(0x5B, "868.95MHz");
            put(0x5C, "433.92MHz RFXtrxIOT");
            put(0x5D, "868.00MHz RFXtrxIOT");
            put(0x5F, "434.50MHz transceiver");
        }
    };
    /**
     * firmware types.
     */
    public static final Map<Integer, String> FIRMWARE_TYPES = new HashMap<Integer, String>() {
        {
            put(0x00, "Type 1 RO");
            put(0x01, "Type 1");
            put(0x02, "Type 2");
            put(0x03, "Ext");
            put(0x04, "Ext 2");
            put(0x05, "Pro 1");
            put(0x06, "Pro 2");
            put(0x10, "ProXL1");
        }
    };

    /**
     * constructor for the generic information presentation.
     */
    public GenericInfoPresentation() {
        super("RFXCom device info", "Information about the RFXCom device's hardware and firmware.");
    }

}
