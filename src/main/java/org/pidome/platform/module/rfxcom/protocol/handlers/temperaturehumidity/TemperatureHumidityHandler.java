/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.temperaturehumidity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.protocol.handlers.DataOnlyHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.OregonDefaultControlNames;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.STATUS_GROUP_NAME;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.VALUES_GROUP_NAME;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DeviceStringAddress;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.presentation.components.list.DescriptionListItem;

/**
 * Handler for temperature and humidity.
 *
 * @author johns
 */
@SuppressWarnings("checkstyle:magicnumber")
public class TemperatureHumidityHandler extends DataOnlyHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(TemperatureHumidityHandler.class);

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType implements ProtocolDefinition {
        /**
         * THGN122/123, THGN132, THGR122/228/238/268.
         */
        TYPE1(1, "THGN122/123, THGN132, THGR122/228/238/268"),
        /**
         * THGR810, THGN800.
         */
        TYPE2(2, "THGR810, THGN800"),
        /**
         * RTGR328.
         */
        TYPE3(3, "RTGR328"),
        /**
         * THGR328.
         */
        TYPE4(4, "THGR328"),
        /**
         * WTGR800.
         */
        TYPE5(5, "WTGR800"),
        /**
         * THGR918, THGRN228, THGN50.
         */
        TYPE6(6, "THGR918, THGRN228, THGN50"),
        /**
         * TFA TS34C, Cresta.
         */
        TYPE7(7, "TFA TS34C, Cresta"),
        /**
         * WT260,WT260H,WT440H,WT450,WT450H.
         */
        TYPE8(8, "WT260,WT260H,WT440H,WT450,WT450H"),
        /**
         * Viking 02035, 02038.
         */
        TYPE9(9, "Viking 02035, 02038"),
        /**
         * Rubicson.
         */
        TYPE10(10, "Rubicson"),
        /**
         * An unknown type.
         */
        UNKNOWN(-1, "Unknown or unhandled type");

        /**
         * Supplying value.
         */
        private final int value;
        /**
         * Description.
         */
        private final String desc;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param bitValue Type value.
         * @param description Type description.
         */
        PacketSubType(final int bitValue, final String description) {
            this.value = bitValue;
            this.desc = description;
        }

        /**
         * @return The enum value
         */
        @Override
        public int getValue() {
            return value;
        }

        /**
         * @return The description.
         */
        @Override
        public String getDescription() {
            return desc;
        }
    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public TemperatureHumidityHandler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {

        final PacketSubType subType = getSubPacketTypeByBitId(getParser().getSubType(), PacketSubType.UNKNOWN, PacketSubType.values());
        final byte[] body = this.getParser().getMessageBody();

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            builder.append(BasicPacketParser.decodeSingleByte(body[i]));
        }

        String deviceId = builder.toString();
        int remoteChannel = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[1]), 16);
        double temp = OregonBasedGenericImpl.getTemperature(body[2], body[3]);

        int hum = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[4]), 16);
        int humstat = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[5]), 16);

        int batSig = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[6]), 16);

        double bat = OregonBasedGenericImpl.getBatteryStatus(batSig);
        double sig = OregonBasedGenericImpl.getSignalStatus(batSig);

        LOG.debug("Address: {}, Channel: {}, Temp: {}, Humidity: {}, Humidity txt const: {}, Battery: {}, Signal: {}", deviceId, remoteChannel, temp, hum, humstat, bat, sig);

        boolean found = false;

        for (DeviceAddress deviceAddress : this.getRunningDeviceAddresses(TemperatureHumidityDevice.class)) {
            if (deviceAddress.getAddress().equals(deviceId)) {
                found = true;
                DeviceDataNotification notification = new DeviceDataNotification(deviceAddress);
                notification.addData(VALUES_GROUP_NAME, OregonBasedGenericImpl.OregonDefaultControlNames.TEMPERATURE.name(), temp);

                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.HUMIDITY.name(), hum);
                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.HUMIDITYTEXT.name(), OregonBasedGenericImpl.getHumidityText(humstat));

                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.BATTERY.name(), bat);
                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.SIGNAL.name(), sig);
                this.sendDataNotification(notification);
            }
        }
        if (!found && this.discoveryEnabled()) {

            DiscoveredDevice<DeviceStringAddress> newDevice = new DiscoveredDevice<>(TemperatureHumidityDevice.class, new StringBuilder(subType.getDescription()).append(" or alike").toString());

            newDevice.addDescriptionListItem(new DescriptionListItem("Channel", remoteChannel));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied temp", temp));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied humudity", hum));
            newDevice.addDescriptionListItem(new DescriptionListItem("Provides", "Temperature and humidity"));
            newDevice.addDescriptionListItem(new DescriptionListItem("packettype", ProtocolCollection.PacketType.TEMPERATURE_HUMIDITY.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("packetsubtype", subType.toString()));

            newDevice.addFieldParameter("packettype", ProtocolCollection.PacketType.TEMPERATURE_HUMIDITY.toString());
            newDevice.addFieldParameter("packetsubtype", subType.toString());

            DeviceStringAddress newAddress = new DeviceStringAddress();
            newAddress.setAddress(deviceId);
            newDevice.setAddress(newAddress);

            this.discovered(newDevice);

        }
    }
}
