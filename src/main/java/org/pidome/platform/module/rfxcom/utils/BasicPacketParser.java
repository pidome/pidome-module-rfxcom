/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection.PacketType;

/**
 * Creates a basic packet structure.
 *
 * @author John
 */
@SuppressWarnings("checkstyle:magicnumber")
public final class BasicPacketParser {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(BasicPacketParser.class);

    /**
     * The bytes collection.
     */
    private final byte[] bytes;

    /**
     * Package full length without first byte.
     */
    private int length;

    /**
     * The current package sequence reported.
     */
    private int sequence;

    /**
     * The packet type.
     */
    private PacketType packetType;

    /**
     * The subtype int reference.
     */
    private int subType;

    /**
     * Constructor.
     *
     * @param packet The bytes to be handled by this parser instance.
     */
    public BasicPacketParser(final byte[] packet) {
        this.bytes = packet;
    }

    /**
     * Decodes a package.
     *
     * @throws BasicPacketParserException On failed parsing.
     */
    public void decode() throws BasicPacketParserException {
        try {
            if (bytes.length - 1 == (bytes[0] & 0xFF)) {
                try {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Decoding for type: {}, sub: {}, sequence: {}", (bytes[1] & 0xFF), (bytes[2] & 0xFF), (bytes[3] & 0xFF));
                    }
                    packetType = ProtocolCollection.getPacketTypes().get((bytes[1] & 0xFF));
                    subType = ((bytes[2] & 0xFF));
                    sequence = (bytes[3] & 0xFF);
                } catch (NullPointerException ex) {
                    throw new BasicPacketParserException("Unsupported protocol, not implemented or unknown", ex);
                }
            } else {
                throw new BasicPacketParserException("Received data differs from reported length. Reported: " + (bytes[0] & 0xFF) + ", real: " + bytes.length);
            }
        } catch (IndexOutOfBoundsException ex) {
            throw new BasicPacketParserException("Invalid package", ex);
        }
    }

    /**
     * Returns the subtype as int.
     *
     * @return The sub type as integer.
     */
    public int getSubType() {
        return subType;
    }

    /**
     * Returns the bytes excluding the length byte.
     *
     * This in fact returns a copy of the original bytes (from byte[1] to
     * byte[length-1]).
     *
     * @return The bytes in this parser.
     */
    public byte[] getBytes() {
        byte[] newArray = new byte[bytes.length - 1];
        System.arraycopy(bytes, 1, newArray, 0, bytes.length - 1);
        return newArray;
    }

    /**
     * This returns the message body.
     *
     * The body is the byte array minus the length, packet type, packet subtype
     * and sequence number.
     *
     * @return byte array of the message body.
     */
    public byte[] getMessageBody() {
        byte[] newArray = new byte[bytes.length - 4];
        System.arraycopy(bytes, 4, newArray, 0, bytes.length - 4);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Message body: ", decodeAllBytes(newArray));
        }
        return newArray;
    }

    /**
     * Returns the package length excluding the length byte.
     *
     * @return The package length minus the length byte.
     */
    public int getLength() {
        return bytes.length - 1;
    }

    /**
     * Returns the byte length including the length byte.
     *
     * @return The absolute length of the full message.
     */
    public int getAbsoluteLength() {
        return bytes.length;
    }

    /**
     * Returns the packet type.
     *
     * @return The packet type.
     */
    public PacketType getPacketType() {
        return this.packetType;
    }

    /**
     * Returns the packet type as hex in String format.
     *
     * @return The package type as string.
     */
    public String getPacketTypeByteChar() {
        return decodeSingleByte(bytes[1]);
    }

    /**
     * Returns the packet subtype as hex in String format.
     *
     * @return subtype in hex string.
     */
    public String getPacketSubTypeByteChar() {
        return decodeSingleByte(bytes[2]);
    }

    /**
     * Returns the current sequence.
     *
     * @return The sequence.
     */
    public int getSequence() {
        return sequence;
    }

    /**
     * Array of possible hex chars.
     */
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    /**
     * Quick to hex parser for debug output.
     *
     * @return full decoded bytes sequence.
     */
    @Override
    public String toString() {
        return decodeAllBytes(bytes);
    }

    /**
     * Decodes all bytes.
     *
     * @param toDecode The byte set to decode.
     * @return String with decoded bytes.
     */
    public static String decodeAllBytes(final byte[] toDecode) {
        char[] hexChars = new char[toDecode.length * 2];
        for (int j = 0; j < toDecode.length; j++) {
            int v = toDecode[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Decodes a single byte to return.
     *
     * @param toDecode The single byte to decode.
     * @return The decoded single byte.
     */
    public static String decodeSingleByte(final byte toDecode) {
        char[] hexChar = new char[2];
        int value = (toDecode & 0xFF);
        hexChar[0] = HEX_ARRAY[value >>> 4];
        hexChar[1] = HEX_ARRAY[value & 0x0F];
        return new String(hexChar);
    }

}
