/**
 * Handlers and devices for combined temperature and humidity based devices.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.temperaturehumidity;
