/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.rain;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.protocol.handlers.DataOnlyHandler;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl;
import org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.OregonDefaultControlNames;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.STATUS_GROUP_NAME;
import static org.pidome.platform.module.rfxcom.utils.OregonBasedGenericImpl.VALUES_GROUP_NAME;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DeviceStringAddress;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.presentation.components.list.DescriptionListItem;

/**
 * Handler for rain based devices, mostly oregon specified.
 *
 * @author John
 */
@SuppressWarnings("checkstyle:magicnumber")
public class RainHandler extends DataOnlyHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(RainHandler.class);

    /**
     * Supported sub packages by this plugin.
     */
    public enum PacketSubType implements ProtocolDefinition {
        /**
         * RGR126/682/918.
         */
        TYPE1(1, "RGR126/682/918"),
        /**
         * PCR800.
         */
        TYPE2(2, "PCR800"),
        /**
         * TFA.
         */
        TYPE3(3, "TFA"),
        /**
         * UPM RG700.
         */
        TYPE4(4, "UPM RG700"),
        /**
         * WS2300.
         */
        TYPE5(5, "WS2300"),
        /**
         * An unknown type.
         */
        UNKNOWN(-1, "Unknown or unhandled type");

        /**
         * Supplying value.
         */
        private final int value;
        /**
         * Description.
         */
        private final String desc;

        /**
         * Enum constructor for supplying a value to the enum.
         *
         * @param bitValue Type value.
         * @param description Type description.
         */
        PacketSubType(final int bitValue, final String description) {
            this.value = bitValue;
            this.desc = description;
        }

        /**
         * @return The enum value
         */
        @Override
        public int getValue() {
            return value;
        }

        /**
         * @return The description.
         */
        @Override
        public String getDescription() {
            return desc;
        }

    }

    /**
     * Constructor.
     *
     * @param module The module.
     * @param parser The parser for this instance.
     */
    public RainHandler(final PidomeRFXCom module, final BasicPacketParser parser) {
        super(module, parser);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void handleReceivedFromDriver() throws PacketProtocolException {

        final PacketSubType subType = getSubPacketTypeByBitId(getParser().getSubType(), PacketSubType.UNKNOWN, PacketSubType.values());
        final byte[] body = this.getParser().getMessageBody();

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            builder.append(BasicPacketParser.decodeSingleByte(body[i]));
        }
        String deviceId = builder.toString();

        int lowRate = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[2]), 16);
        int highRate = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[3]), 16);

        float currentRate = 0;

        if (subType.equals(PacketSubType.TYPE1)) {
            currentRate = (highRate * 0x100) + lowRate;
        } else if (subType.equals(PacketSubType.TYPE2)) {
            currentRate = ((highRate * 0x100) + lowRate) / 100;
        }

        int totalRate1 = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[4]), 16);
        int totalRate2 = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[5]), 16);
        int totalRate3 = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[6]), 16);

        float totalRate = ((totalRate1 * 0x1000) + (totalRate2 * 0x100) + totalRate3) / 10;

        int batSig = Integer.parseInt(BasicPacketParser.decodeSingleByte(body[7]), 16);

        double bat = OregonBasedGenericImpl.getBatteryStatus(batSig);
        double sig = OregonBasedGenericImpl.getSignalStatus(batSig);

        boolean found = false;

        LOG.debug("Type: {}, Address: {}, Current rate: {} mm/h, Total rate: {} mm, Battery: {}, Signal: {}", subType.getDescription(), deviceId, currentRate, totalRate, bat, sig);

        for (DeviceAddress deviceAddress : this.getRunningDeviceAddresses(RainDevice.class)) {
            if (deviceAddress.getAddress().equals(deviceId)) {
                found = true;
                DeviceDataNotification notification = new DeviceDataNotification(deviceAddress);

                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.RAIN_TOTAL.name(), totalRate);
                notification.addData(VALUES_GROUP_NAME, OregonDefaultControlNames.RAIN_CURRENT.name(), currentRate);

                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.BATTERY.name(), bat);
                notification.addData(STATUS_GROUP_NAME, OregonDefaultControlNames.SIGNAL.name(), sig);

                this.sendDataNotification(notification);
            }
        }
        if (!found && this.discoveryEnabled()) {

            DiscoveredDevice<DeviceStringAddress> newDevice = new DiscoveredDevice<>(RainDevice.class, new StringBuilder(subType.getDescription()).append(" or alike").toString());

            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied current", currentRate));
            newDevice.addDescriptionListItem(new DescriptionListItem("Supplied total", totalRate));
            newDevice.addDescriptionListItem(new DescriptionListItem("Provides", "Rain information"));
            newDevice.addDescriptionListItem(new DescriptionListItem("packettype", ProtocolCollection.PacketType.RAIN.toString()));
            newDevice.addDescriptionListItem(new DescriptionListItem("packetsubtype", subType.toString()));

            newDevice.addFieldParameter("packettype", ProtocolCollection.PacketType.RAIN.toString());
            newDevice.addFieldParameter("packetsubtype", subType.toString());

            DeviceStringAddress address = new DeviceStringAddress();
            address.setAddress(deviceId);
            newDevice.setAddress(address);

            this.discovered(newDevice);

        }
    }

}
