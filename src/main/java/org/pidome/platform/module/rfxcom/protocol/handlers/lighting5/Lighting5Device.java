/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.lighting5;

import org.pidome.platform.module.rfxcom.protocol.AbstractRFXComDevice;
import org.pidome.platform.module.rfxcom.protocol.ProtocolCollection;
import org.pidome.platform.modules.devices.DeviceStringAddress;

/**
 * A lighting 5 device.
 *
 * @author John
 */
public class Lighting5Device extends AbstractRFXComDevice<DeviceStringAddress> {

    /**
     * The subtype.
     */
    private Lighting5Handler.PacketSubType subType = Lighting5Handler.PacketSubType.LIGHTWAVERF;

    /**
     * Returns the packet sub type.
     *
     * @return The subtype as int.
     */
    @Override
    public int getPacketSubType() {
        return subType.getValue();
    }

    /**
     * Returns the packet type.
     *
     * @return The main protocol.
     */
    @Override
    public ProtocolCollection.PacketType getPacketType() {
        return ProtocolCollection.PacketType.LIGHTING_5;
    }

    /**
     * If there are any tasks or what so ever used, use this to stop them.
     */
    @Override
    public void shutdownDevice() {
        ///Not used.
    }

    /**
     * If there are any automated tasks or what so ever use this to start them.
     */
    @Override
    public void startupDevice() {
        this.getParameters().get("subtype").ifPresentOrElse(parameter -> {
            this.subType = Lighting5Handler.PacketSubType.valueOf(parameter.getValue());
        }, () -> {
            this.subType = Lighting5Handler.PacketSubType.UNKNOWN;
        });
    }

}
