/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.RFXComSendCommand;
import org.pidome.platform.module.rfxcom.protocol.PacketProtocolException;
import org.pidome.platform.module.rfxcom.protocol.ProtocolDefinition;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.modules.devices.Device;
import org.pidome.platform.modules.devices.DeviceAddress;
import org.pidome.platform.modules.devices.DeviceCommandRequest;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;

/**
 * Minimal available functions per protocol.
 *
 * @author John
 */
public abstract class AbstractProtocolHandler {

    /**
     * The parser. Only available when there is data received from the hardware.
     */
    private BasicPacketParser parser;

    /**
     * The module.
     */
    private PidomeRFXCom module;

    /**
     * The command issued by the device.
     */
    private DeviceCommandRequest request;

    /**
     * Parser constructor.
     *
     * @param rfxcomModule The module.
     * @param packetParser The parser.
     */
    public AbstractProtocolHandler(final PidomeRFXCom rfxcomModule, final BasicPacketParser packetParser) {
        this.module = rfxcomModule;
        this.parser = packetParser;
    }

    /**
     * USed when command is received.
     *
     * @param rfxcomModule the module.
     * @param commandRequest The request done.
     */
    public AbstractProtocolHandler(final PidomeRFXCom rfxcomModule, final DeviceCommandRequest commandRequest) {
        this.module = rfxcomModule;
        this.request = commandRequest;
    }

    /**
     * Sends data to the RFXCom.
     *
     * This method supports sending delayed messages. These are handled FIFO.
     *
     * @param notification The device data update notification.
     */
    protected final void sendDataNotification(final DeviceDataNotification notification) {
        this.module.sendDeviceDataNotification(notification);
    }

    /**
     * Sends the data to the RFXCom.
     *
     * @param sendCommand The command to send.
     */
    protected final void sendToRFXCom(final RFXComSendCommand sendCommand) {
        this.module.sendToRFXCom(sendCommand);
    }

    /**
     * Returns a list of device addresses of the given device class type.
     *
     * @param deviceClass the device class to return the addresses for.
     * @return A list of device addresses.
     */
    protected final List<DeviceAddress> getRunningDeviceAddresses(final Class<? extends Device> deviceClass) {
        return this.module.getRunningDevices().getOrDefault(deviceClass, new ArrayList<>());
    }

    /**
     * Returns true or false depending on if discovery is enabled.
     *
     * @return true when discovery is enabled.
     */
    protected final boolean discoveryEnabled() {
        return this.module.isDiscoveryEnabled();
    }

    /**
     * Registers a newly discovered device.
     *
     * @param device The device discovered.
     */
    protected final void discovered(final DiscoveredDevice device) {
        this.module.discovered(device);
    }

    /**
     * Returns the request issued by the device.
     *
     * Only available when a request is made based on a device.
     *
     * @return The command from a device.
     */
    protected final DeviceCommandRequest getCommand() {
        return this.request;
    }

    /**
     * Returns the parser.
     *
     * @return The parser containing the data.
     */
    protected final BasicPacketParser getParser() {
        return this.parser;
    }

    /**
     * Handles data received from rfxcom.
     *
     * @throws PacketProtocolException When driver handled data fails to be
     * handled.
     */
    public abstract void handleReceivedFromDriver() throws PacketProtocolException;

    /**
     * Handles data received from device.
     *
     * @param command The command send from a device.
     * @param promise to fulfil what is received from a device was successfully
     * handled.
     * @return The future to identify it succeeded or failed.
     */
    public abstract Future<Void> handleReceivedFromDevice(DeviceCommandRequest command, Promise<Void> promise);

    /**
     * Generic method to create a subtype map.
     *
     * @param <E> The subtype enum.
     * @param subType The subtype to create the map for.
     * @return Map based on the given subTypes
     */
    @SafeVarargs
    public static <E extends ProtocolDefinition> Map<Integer, E> createUnmodifiableProtocolMap(final E... subType) {
        Map<Integer, E> baseMap = new HashMap<>();
        for (E type : subType) {
            baseMap.put(type.getValue(), type);
        }
        return Collections.unmodifiableMap(baseMap);
    }

    /**
     * Returns a sub packet integer type by String id.
     *
     * @param <E> The type to use.
     * @param id The id to retrieve.
     * @param defaultValue The default when not found.
     * @param subTypes The collection of subtypes of E
     * @return The id of the protocol subtype.
     */
    @SafeVarargs
    public static <E extends ProtocolDefinition> int getSubPacketTypeIntByStringId(final String id, final E defaultValue, final E... subTypes) {
        return getSubPacketTypeByStringId(id, defaultValue, subTypes).getValue();
    }

    /**
     * Returns a sub packet integer type by String id.
     *
     * @param <E> The type to use.
     * @param id The id to retrieve.
     * @param defaultValue The default when not found.
     * @param subTypes The collection of subtypes of E
     * @return The id of the protocol subtype.
     */
    @SafeVarargs
    public static <E extends ProtocolDefinition> E getSubPacketTypeByStringId(final String id, final E defaultValue, final E... subTypes) {
        for (E type : subTypes) {
            if (type.name().equals(id)) {
                return type;
            }
        }
        return defaultValue;
    }

    /**
     * Returns a sub packet integer type by String id.
     *
     * @param <E> The type to use.
     * @param bit The id to retrieve.
     * @param defaultValue The default when not found.
     * @param subTypes The collection of subtypes of E
     * @return The id of the protocol subtype.
     */
    @SafeVarargs
    public static <E extends ProtocolDefinition> E getSubPacketTypeByBitId(final int bit, final E defaultValue, final E... subTypes) {
        for (E type : subTypes) {
            if (type.getValue() == bit) {
                return type;
            }
        }
        return defaultValue;
    }

}
