/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.module.rfxcom.protocol.handlers.rfxcom;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.pidome.platform.module.rfxcom.PidomeRFXCom;
import org.pidome.platform.module.rfxcom.presentations.GenericInfoPresentation;
import org.pidome.platform.module.rfxcom.utils.BasicPacketParser;
import org.pidome.platform.presentation.components.list.DescriptionList;
import org.pidome.platform.presentation.components.list.DescriptionListItem;
import org.pidome.platform.presentation.skeleton.PresentationSection;
import org.pidome.tools.utilities.Serialization;

/**
 *
 * @author johns
 */
public class RFXComHandlerTest {

    public RFXComHandlerTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getRFXComInfo method, of class RFXComHandler.
     */
    @Test
    public void testGetRFXComInfo() throws Exception {
        PidomeRFXCom rfxCom = new PidomeRFXCom();
        byte[] statusReport = new byte[]{
            /// Headers
            0x14, 0x01, 0x00, 0x01,
            /// Body
            0x02, 0x53, 0x22, 0x00, 0x00, 0x27, 0x00, 0x03, 0x00, 0x1C, 0x10, 0x5C, 0x00, 0x00, 0x00, 0x00, 0x00
        };

        BasicPacketParser parser = new BasicPacketParser(statusReport);

        try {
            parser.decode();

            RFXComHandler instance = new RFXComHandler(false, rfxCom, parser);
            instance.handleReceivedFromDriver();

            assertThat(instance.hasRFXComInfo(), is(true));
            GenericInfoPresentation result = instance.getRFXComInfo();

            boolean walkedDeviceTree = false;

            for (PresentationSection section : result.getSections()) {
                if (section instanceof DescriptionList) {
                    for (DescriptionListItem item : ((DescriptionList) section).getListItems()) {
                        switch (item.getDt()) {
                            case "Device type":
                                assertThat(item.getDd(), is("433.92MHz transceiver"));
                                walkedDeviceTree = true;
                                break;
                            case "Hardware version":
                                assertThat(item.getDd(), is("3.0, ProXL1"));
                                walkedDeviceTree = true;
                                break;
                            case "Transmitter power":
                                assertThat(item.getDd(), is("10 dBm"));
                                walkedDeviceTree = true;
                                break;
                            case "Noise":
                                assertThat(item.getDd(), is("92 dBm"));
                                walkedDeviceTree = true;
                                break;
                            case "Firmware version":
                                assertThat(item.getDd(), is(1034));
                                walkedDeviceTree = true;
                                break;
                            /// Two protocola test:
                            case "Oregon Scientific":
                                assertThat(item.getDd(), is(true));
                                walkedDeviceTree = true;
                                break;
                            case "FS20":
                                assertThat(item.getDd(), is(false));
                                walkedDeviceTree = true;
                                break;
                        }
                    }
                }
            }

            System.out.println(Serialization.getDefaultObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(result));

            assertThat(walkedDeviceTree, is(true));

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println(BasicPacketParser.decodeAllBytes(parser.getBytes()));
            throw (ex);
        }
    }

}
